# Team KTH:
1. Kaspar Ranna
2. Taido Purasson
3. Heigo Ers

## Homework 1:
[User stories](https://bitbucket.org/taidopurason/lg3-kth/wiki/Homework%201)

## Homework 2:
[Use Cases](https://bitbucket.org/taidopurason/lg3-kth/wiki/Use%20Cases)

[Functional Requirements and Issues](https://bitbucket.org/taidopurason/lg3-kth/wiki/Functional%20requirements%20and%20issues)

[Class model](https://bitbucket.org/taidopurason/lg3-kth/wiki/POS%20class%20model)

## Homework 3:
[Link to Homework 3](https://bitbucket.org/taidopurason/lg3-kth/commits/tag/homework-3)

## Homework 4:
[The debugger task answers](https://bitbucket.org/taidopurason/lg3-kth/wiki/Lab%204%20The%20debugger%20task)

[Link to Homework 4](https://bitbucket.org/taidopurason/lg3-kth/commits/tag/homework-4)

## Homework 5:
[Link to Homework 5](https://bitbucket.org/taidopurason/lg3-kth/commits/tag/homework-5)

## Homework 6:

.[Link to Homework 6 Tag](https://bitbucket.org/taidopurason/lg3-kth/commits/tag/homework-6)

[Link to changes made to Refractor code](https://docs.google.com/document/d/1RBOHMJ2kWSHhpuIzSrkLx4NAVudi32_r6BlKoRscLgs/edit?usp=sharing)

[Link to Test Cases](https://docs.google.com/document/d/1MuPq8g37jJMuc6xSDLDCbnnvtEBCe6Oi0YQOzmOQJgs/edit?usp=sharing)

[Link to Test Plan](https://docs.google.com/spreadsheets/d/1Lhn3hb6eP8vIhKoU51jzigJtm-pT6BHnBMQwq6KD2s8/edit?usp=sharing)

## Homework 7:

.[Link to Homework 7 Tag](https://bitbucket.org/taidopurason/lg3-kth/commits/tag/homework-7)

[Link to SaleSystemCLI distribution](https://drive.google.com/open?id=1Gd8i1G0dTvlgEmaanVbO-i1TdCewvzIY)

[Link to SaleSystemGUI distribution](https://drive.google.com/open?id=1bm5Au1tw8RIBFaIUpw0WK4f89cNWxrZR)

[Link to created usability tests](https://docs.google.com/document/d/1xhn98cf4i48q_jx2o4hldFkMohNyAEUg7LUYv6mFGOg/edit?usp=sharing)

[Link to executed test cases](https://docs.google.com/document/d/1TJlcrfsLpz0GsXRxpjlYiapCc9wvuOh5oYkAykmGGVQ/edit?usp=sharing)

[Link to Test plan with test results](https://docs.google.com/spreadsheets/d/1maPKm0YnLBFqht5UoeS-z2XA37VRjZexL8CJcFs4AGU/edit?usp=sharing)

[Link to MMR application usability testing results](https://docs.google.com/document/d/1e19F-UD4kMqw6nl_VrmGYCnSRlYzAYZqoujY2d1UJKo/edit?usp=sharing)

