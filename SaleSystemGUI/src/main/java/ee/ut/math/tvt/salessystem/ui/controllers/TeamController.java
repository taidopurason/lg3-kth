package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.dataobjects.Team;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    private Team meeskond;
    private static final Logger log = LogManager.getLogger(TeamController.class);


    @FXML
    private Label teamName;
    @FXML
    private Label contactName;
    @FXML
    private Label contactEmail;

    @FXML
    private Label member1Name;
    @FXML
    private Label member2Name;
    @FXML
    private Label member3Name;

    @FXML
    private Label motto;
    @FXML
    private ImageView image;
    @FXML
    private ListView<String> memberListView;
    @FXML
    private Button memberConfirmButton;
    @FXML
    private Button memberCancelButton;
    @FXML
    private Button editMemberButton;
    @FXML
    private Button removeMemberButton;
    @FXML
    private TextField changeMemberTextField;
    @FXML
    private Label editInfoLabel;
    @FXML
    private GridPane editGridPane;

    private ObservableList<String> memberListViewItems;

    private boolean confirmMode = false;
    private boolean removeMode = false;
    private boolean changeMode = false;

    //Constructor
    public TeamController(String dataPath){

        try{
            this.meeskond = new Team(dataPath);
        }
        catch(IOException e){
            log.error("application.properties not found");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        log.debug("initializing TeamController ");
        //Assign right text to right label

        teamName.setText(meeskond.getTeamName());
        contactName.setText(meeskond.getContactPerson());
        contactEmail.setText(meeskond.geteMail());


        String[] members = meeskond.getMembers().split(",");

        member1Name.setText(members[0]);
        member2Name.setText(members[1]);
        member3Name.setText(members[2]);

        ArrayList<String> memberArrayList = new ArrayList<String>();
        for (String member : members) {
            memberArrayList.add(member);
        }
        memberListViewItems = FXCollections.observableArrayList(memberArrayList);
        memberListView.setItems(memberListViewItems);

        memberListView.getSelectionModel().select(null); //null for no selection right at the start.

        motto.setText(meeskond.getMotto());

        File logoFile = new File(meeskond.getImagePath());
        Image logo = null;
        try{
            logo = new Image(logoFile.toURI().toString());
        }
        catch(IllegalArgumentException e){
            log.error("Team logo not found");
        }

        image.setImage(logo);

        editMemberButton.setDisable(true);
        editMemberButton.setVisible(false);
        removeMemberButton.setDisable(true);
        removeMemberButton.setVisible(false);


    }

    public void editMemberButtonClicked () {
        //If nothing was selected, it selects the 0 element from the team member list.
        if (memberListView.getSelectionModel().getSelectedIndex() == -1) {
            memberListView.getSelectionModel().select(0);
        }
        //What if there are no more team members left in the team?
        changeMode = true;
        setConfirmationMode(true);
        editInfoLabel.setText("Change team member info?");
        changeMemberTextField.setText(memberListView.getSelectionModel().getSelectedItem());
    }

    public void removeMemberButtonClicked () {
        //If nothing was selected, it selects the 0 element from the team member list.
        if (memberListView.getSelectionModel().getSelectedIndex() == -1) {
            memberListView.getSelectionModel().select(0);
        }
        //What if there are no more team members left in the team?
        removeMode = true;
        setConfirmationMode(true);
        changeMemberTextField.setDisable(true); //specially needs to be disabled.
        changeMemberTextField.setText(memberListView.getSelectionModel().getSelectedItem());
        editInfoLabel.setText("Remove team member?");
    }

    public void memberConfirmButtonClicked () {
        if (removeMode) {
            //removes the element from the observablelist. should be dao? also allows the textbow again.
            memberListViewItems.remove(memberListView.getSelectionModel().getSelectedItem());
            changeMemberTextField.setDisable(false);
        }
        if (changeMode) {
            //changes the element in the observablelist for now. Should be dao?
            int index = memberListViewItems.indexOf(memberListView.getSelectionModel().getSelectedItem());
            memberListViewItems.set(index, changeMemberTextField.getText());
        }
        changeMemberTextField.setText("");
        removeMode = false;
        changeMode = false;
        setConfirmationMode(false);
    }

    public void memberCancelButtonClicked () {
        if (removeMode) {
            changeMemberTextField.setDisable(false);
        }
        changeMemberTextField.setText("");
        removeMode = false;
        changeMode = false;
        setConfirmationMode(false);
    }

    public void setConfirmationMode(boolean value) {
        //buttons near listview
        editMemberButton.setVisible(!value);
        editMemberButton.setDisable(value);
        removeMemberButton.setVisible(!value);
        removeMemberButton.setDisable(value);
        //whole gridpane with textfield, label and confirm&cancel
        editGridPane.setVisible(value);
        editGridPane.setDisable(!value);
        //the list with the team member names
        memberListView.setDisable(value);

    }

}


