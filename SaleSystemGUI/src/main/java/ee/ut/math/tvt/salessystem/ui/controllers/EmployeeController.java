package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Employee;

import ee.ut.math.tvt.salessystem.logic.EmployeeLogic;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class EmployeeController implements Initializable {

    private boolean editMode = false;

    @FXML
    private Label employeeNameLabel;
    @FXML
    private Label employeePositionLabel;
    @FXML
    private Label employeeAddressLabel;
    @FXML
    private Label employeePhoneNumberLabel;
    @FXML
    private Label employeeEmailLabel;
    @FXML
    private TextField employeeNameTextField;
    @FXML
    private TextField employeePositionTextField;
    @FXML
    private TextField employeeAddressTextField;
    @FXML
    private TextField employeePhoneNumberTextField;
    @FXML
    private TextField employeeEmailTextField;
    @FXML
    private ListView<Employee> employeeListView;
    @FXML
    private Button employeeEditButton;
    @FXML
    private Button employeeDeleteButton;

    //Constructor
    private final EmployeeLogic employeeLogic;

    public EmployeeController(EmployeeLogic employee){
        this.employeeLogic = employee;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Employee> employeeListList = FXCollections.observableArrayList(employeeLogic.getEmployeeList());
        employeeListView.setItems(employeeListList);
        employeeListView.getSelectionModel().select(0); //parameter as "null" if you want to start it out with no auto-selected row.
        fillLabels(employeeListView.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void employeeListViewClicked() {
        setEditModeFalse();
        fillLabels(employeeListView.getSelectionModel().getSelectedItem());
    }

    public void editButtonClicked() {
        if (editMode) {
            Employee selectedEmployee = employeeListView.getSelectionModel().getSelectedItem();
            employeeLogic.editEmployeeData(selectedEmployee.getID(), employeeNameTextField.getText(), employeePositionTextField.getText(), employeeAddressTextField.getText(), employeeEmailTextField.getText(), employeePhoneNumberTextField.getText());
            employeeListView.setItems(FXCollections.observableArrayList(employeeLogic.getEmployeeList()));
            setEditModeFalse();
            fillLabels(selectedEmployee);
        } else {
            setEditModeTrue();
        }
    }

    public void deleteButtonClicked() {
        employeeLogic.removeEmployee(employeeListView.getSelectionModel().getSelectedItem());
        employeeListView.setItems(FXCollections.observableArrayList(employeeLogic.getEmployeeList()));
        employeeListView.getSelectionModel().select(0);
        clearTextFields();
        setEditModeFalse();
    }

    public void setEditModeTrue () {
        editMode = true;
        setInfoElementStates(false);
        fillTextFields(employeeListView.getSelectionModel().getSelectedItem());
    }

    public void setEditModeFalse () {
        editMode = false;
        setInfoElementStates(true);
        clearTextFields();
    }

    public void clearTextFields ()  {
        employeeNameTextField.setText("");
        employeePositionTextField.setText("");
        employeeAddressTextField.setText("");
        employeePhoneNumberTextField.setText("");
        employeeEmailTextField.setText("");
    }

    public void fillTextFields (Employee selectedEmployee) {
        employeeNameTextField.setText(selectedEmployee.getName());
        employeePositionTextField.setText(selectedEmployee.getPosition());
        employeeAddressTextField.setText(selectedEmployee.getAddress());
        employeePhoneNumberTextField.setText(selectedEmployee.getPhoneNumber());
        employeeEmailTextField.setText(selectedEmployee.getEmail());
    }

    public void fillLabels (Employee selectedEmployee) {
        employeeNameLabel.setText(selectedEmployee.getName());
        employeePositionLabel.setText(selectedEmployee.getPosition());
        employeeAddressLabel.setText(selectedEmployee.getAddress());
        employeePhoneNumberLabel.setText(selectedEmployee.getPhoneNumber());
        employeeEmailLabel.setText(selectedEmployee.getEmail());
    }

    public void setInfoElementStates (boolean value) {
        //If it is in edit mode, the same button is reused as the save button.
        if (value) {
            employeeEditButton.setText("Edit");
        } else {
            employeeEditButton.setText("Save");
        }
        //Sets the other labels and textfields visible and enabled, when user wants to edit, otherwise disables and hides them.
        employeeDeleteButton.setDisable(value);
        employeeDeleteButton.setVisible(!value);

        employeeNameLabel.setVisible(value);
        employeePositionLabel.setVisible(value);
        employeeAddressLabel.setVisible(value);
        employeePhoneNumberLabel.setVisible(value);
        employeeEmailLabel.setVisible(value);

        employeeNameTextField.setVisible(!value);
        employeePositionTextField.setVisible(!value);
        employeeAddressTextField.setVisible(!value);
        employeePhoneNumberTextField.setVisible(!value);
        employeeEmailTextField.setVisible(!value);

        employeeNameTextField.setDisable(value);
        employeePositionTextField.setDisable(value);
        employeeAddressTextField.setDisable(value);
        employeePhoneNumberTextField.setDisable(value);
        employeeEmailTextField.setDisable(value);
    }

}


