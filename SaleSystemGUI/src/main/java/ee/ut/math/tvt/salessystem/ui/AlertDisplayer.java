package ee.ut.math.tvt.salessystem.ui;

import javafx.scene.control.Alert;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AlertDisplayer {
    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);
    private Alert alert = new Alert(Alert.AlertType.WARNING);

    public AlertDisplayer() {
        log.debug("Initializing alert displayer.");
        alert.getDialogPane().getStylesheets().add(getClass().getResource("DefaultTheme.css").toExternalForm());
    }

    public void display(String title, String header, String text){
        log.debug("Showing Alert to notify user about: "+header);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(text);
        alert.showAndWait();
    }
}
