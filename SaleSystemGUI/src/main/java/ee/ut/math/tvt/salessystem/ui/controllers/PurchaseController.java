
package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Customer;
import ee.ut.math.tvt.salessystem.logic.Payment;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.SalesSystemUI;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ee.ut.math.tvt.salessystem.ui.AlertDisplayer;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

//    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;
    private final CustomerController customerController;
    private final Warehouse warehouse;
    private final AlertDisplayer alert = new AlertDisplayer();
    private final StockController stockContr;

    @FXML
    private TitledPane purchasePane;
    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button removeItemButton;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private TitledPane shoppingCartPane;
    @FXML
    private Label purchaseInStockLabel;
    @FXML
    private TableColumn<SoldItem, SoldItem> removeCol;
    @FXML
    private TableColumn<SoldItem, SoldItem> discountCol;
    @FXML
    private TableColumn<SoldItem, SoldItem> totalCol;
    @FXML
    private TableColumn<SoldItem, Integer> quantityCol;
    @FXML
    private Text totalTxt;
    @FXML
    private TextField customerIDField;
    @FXML
    private TextField customerNameField;
    @FXML
    private Button checkCustomerButton;
    @FXML
    private TitledPane loyalCustomerPane;
    @FXML
    private Accordion accordion;
    @FXML
    private Text loyalCustomerTxt;

    private ContextMenu suggestion = new ContextMenu();


    public PurchaseController(Warehouse warehouse, ShoppingCart shoppingCart, CustomerController customerContr, StockController stockContr) {
        this.warehouse = warehouse;
        this.shoppingCart = shoppingCart;
        this.customerController = customerContr;
        this.stockContr = stockContr;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.debug("initializing PurchaseController");
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        disableProductField(true);

        loyalCustomerPane.setCollapsible(false);
        accordion.setExpandedPane(purchasePane);


        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsByBarcode();
                }
            }
        });

        this.nameField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsByName();
                }
            }
        });

        purchaseTableView.setEditable(true);
        quantityCol.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        quantityCol.setEditable(true);
        quantityCol.setOnEditCommit(e -> {
            try {
                log.debug("Quantity column edited");
                shoppingCart.editQuantity(e.getRowValue(), e.getNewValue());
            } catch (SalesSystemException err) {

                alert.display("Error", err.getMessage(), "");
            }
            refreshSoldItems();
        });

        discountCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));

        discountCol.setCellFactory(param -> new TableCell<SoldItem, SoldItem>() {
            @Override
            protected void updateItem(SoldItem item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.valueOf(item.calculateDiscount()));
                }
            }
        });

        totalCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));

        totalCol.setCellFactory(param -> new TableCell<SoldItem, SoldItem>() {
            @Override
            protected void updateItem(SoldItem item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.valueOf(item.getTotalPrice()));
                }
            }
        });

        removeCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        removeCol.setCellFactory(param -> new TableCell<SoldItem, SoldItem>() {
            private final Button deleteButton = new Button("-");

            @Override
            protected void updateItem(SoldItem item, boolean empty) {
                super.updateItem(item, empty);
                deleteButton.setStyle("-fx-text-fill: main_color;");
                deleteButton.setOnMouseEntered(e -> deleteButton.setStyle("-fx-text-fill: opposite_text_color;"));
                deleteButton.setOnMouseExited(e -> deleteButton.setStyle("-fx-text-fill: main_color;"));

                if (item == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(
                        event -> {
                            shoppingCart.removeItem(item);
                            refreshSoldItems();
                        }
                );
            }
        });

        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        refreshSoldItems();

        // Name suggestions
        nameField.setContextMenu(suggestion);


        priceField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*\\.?\\d*")) {
                    newValue = newValue.replaceAll("[^\\d.]", "");
                    priceField.setText(newValue);
                }
                if (!newValue.matches("\\d*\\.?\\d*")) {
                    String[] values = newValue.split("\\.", -1);
                    StringBuilder sb = new StringBuilder(values[0]);
                    if (values.length > 1) {
                        sb.append(".").append(values[1]);
                        for (int i = 2; i < values.length; i++) {
                            sb.append(values[i]);
                        }
                    }
                    priceField.setText(sb.toString());
                }
            }
        });
        for (TextField textField : new TextField[]{barCodeField, quantityField}) {
            textField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue,
                                    String newValue) {
                    if (!newValue.matches("\\d*")) {
                        textField.setText(newValue.replaceAll("[^\\d]", ""));
                    }
                }
            });
        }
    }

    private void fillInputsByName() {
        StockItem stockItem = null;
        if (!nameField.getText().isEmpty()) stockItem = warehouse.fetchStockItem(nameField.getText());
        if (stockItem != null) {
            fillInputsByStockItem(stockItem);
        }
    }

    private void fillInputsByBarcode() {
        StockItem stockItem = null;
        if (!barCodeField.getText().isEmpty()) stockItem = warehouse.fetchStockItem(getBarcodeFromField());
        if (stockItem != null) {
            fillInputsByStockItem(stockItem);
        }
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        loyalCustomerTxt.setText("Loyal Customer not registered");
        log.debug("newPurchaseButton clicked");
        loyalCustomerPane.setCollapsible(true);
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void fillInputsBySoldItem(SoldItem soldItem) {
        if (soldItem == null) {
            resetProductField();
        } else {
            log.debug(soldItem.getName() + "selected in table");
            barCodeField.setText(Long.toString(soldItem.getId()));
            quantityField.setText(Integer.toString(soldItem.getQuantity()));
            nameField.setText(soldItem.getName());
            priceField.setText(Double.toString(soldItem.getPrice()));
        }

    }

    @FXML
    public void tableRowClicked() {

        SoldItem chosenItem = purchaseTableView.getSelectionModel().getSelectedItem();
        log.debug(chosenItem.getName() + " row chosen in PurchaseController");
        if (chosenItem != null) {
            fillInputsBySoldItem(chosenItem);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.debug("cancelPurchaseButton clicked");
        try {
            shoppingCart.cancelCurrentPurchase();
            loyalCustomerTxt.setText("Loyal Customer not registered");
            disableInputs();
            refreshSoldItems();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.debug("submitPurchaseButton clicked");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());

            //Payment class for procedure
            Payment pay = new Payment(shoppingCart.calculatePrice());

            //Payment window creation and fxml loading
            Parent parent = null;
            PaymentController paymentController = new PaymentController(pay);
            try {
                log.debug("Initialzing Payment window");

                Parent root = (Parent) loadControls("PaymentChoice.fxml", paymentController);
                Stage paymentStage = new Stage();
                Scene paymentScene = new Scene(root);
                try {
                    paymentScene.getStylesheets().add(SalesSystemUI.class.getResource("DefaultTheme.css").toExternalForm());
                } catch (Exception e) {
                    log.error("Loading Stylesheet failed");
                }
                paymentStage.setScene(paymentScene);
                //Show window and wait until it is closed
                paymentStage.showAndWait();
                log.debug("Payment procedure finished");
            } catch (Exception e) {

            }
            //Check if payment was successful, if yes submitCurrentPurchase
            if (pay.isPaymentSuccess()) {

                log.debug("Payment successful");
                shoppingCart.submitCurrentPurchase(pay.cashOrCredit());
                stockContr.refreshStockItems();
                //Print receipt if requested (currently to terminal)
                if(paymentController.getPrintReceipt()){
                    System.out.println("===============================================");
                    System.out.println(shoppingCart.getLastPurchase().printReceipt());
                    System.out.println("===============================================");
                }
                disableInputs();
                loyalCustomerTxt.setText("Loyal Customer not registered");
                refreshSoldItems();
            }

        } catch (SalesSystemException e) {
            log.debug(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsByStockItem(StockItem stockItem) {
        barCodeField.setText(String.valueOf(stockItem.getId()));
        nameField.setText(stockItem.getName());
        priceField.setText(String.valueOf(stockItem.getPrice()));
        //just so it displays if it's in stock or not
        purchaseInStockLabel.setVisible(true);
        purchaseInStockLabel.setText("In stock: " + stockItem.getQuantity());
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
/*    private StockItem getStockItemByBarcode() {
        log.debug("Searching item by bar code");
        try {
            return dao.findStockItem(getBarcodeFromField());
        } catch (NumberFormatException e) {
            log.debug("Parsing of the bar code was unsuccessful");
            throw e;
        }
    }*/

    private double getPriceFromField() {
        try {
            return Double.parseDouble(priceField.getText());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Price is in the wrong format");
        }
    }

    private int getQuantityFromField() {
        try {
            return Integer.parseInt(quantityField.getText());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Quantity is in the wrong format");
        }
    }

    private Long getBarcodeFromField() {
        try {
            return Long.parseLong(barCodeField.getText());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Barcode is in the wrong format");
        }
    }

    //for future barcode scanner support
    public void fillFieldsFromBarcode(Long barcode) {
        StockItem stockItem = warehouse.fetchStockItem(barcode);
        if (stockItem != null) fillInputsByStockItem(stockItem);
        else {
            log.debug("Stockitem with this barcode can't be found.");
            alert.display("Error", "Stockitem with this barcode can't be found.", "");
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        log.debug("Adding item to Shopping Cart");
        try {
            log.debug("Parsing the fields of product");
            StockItem stockItem = warehouse.fetchStockItem(getBarcodeFromField());
            //if item to be added quantity exceeds the stock quantity it then doesnt allow it to be added in to the shopping cart.
            //Maybe delete this, since Math.min while adding quantity to the consturctor of solditem does the same thing?
            shoppingCart.addItem(new SoldItem(stockItem, getQuantityFromField(), getPriceFromField()));
        } catch (NumberFormatException | SalesSystemException e) {
            log.debug("Exception occurred when adding item: " + e.getMessage());
            alert.display("Error", e.getMessage(), "");
        }
        refreshSoldItems();
        resetProductField();

    }

    private void refreshSoldItems() {
        log.debug("Refreshing SoldItems table");
        customerController.refreshSoldItems();
        totalTxt.setText(Float.toString(shoppingCart.calculatePrice()));
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        purchaseTableView.refresh();
    }

    @FXML
    public void removeItemEventHandler() {
        log.debug("Removing item to Shopping Cart");
        try {
            log.debug("Parsing the fields of product");
            StockItem stockItem = warehouse.fetchStockItem(getBarcodeFromField());
            shoppingCart.removeItem(new SoldItem(stockItem, getQuantityFromField(), getPriceFromField()));
        } catch (NumberFormatException | SalesSystemException e) {
            log.debug("Exception occurred when removing item: " + e.getMessage());
            alert.display("Error", e.getMessage(), "");
        }
        refreshSoldItems();
        resetProductField();
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.removeItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        log.debug("Resetting product fields");
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setText("");
        priceField.setText("");
        purchaseInStockLabel.setVisible(false);
        purchaseInStockLabel.setText("");
    }

    //Actions when loyal costumer button clicked
    @FXML
    private void checkCustomerButtonClicked() {
        log.debug("Checking if customer is in database");
        Customer checkedCustomer;
        if(customerIDField.getText().equals("")){
            checkedCustomer = shoppingCart.checkCustomer(new Customer(customerNameField.getText(),
                    Long.parseLong("-1"), ""));
        }
        else{
            checkedCustomer = shoppingCart.checkCustomer(new Customer(customerNameField.getText(),
                    Long.parseLong(customerIDField.getText()), ""));
        }
        if (checkedCustomer != null) {
            log.debug("Customer found");
            refreshSoldItems();
            loyalCustomerPane.setExpanded(false);
            accordion.setExpandedPane(purchasePane);
            loyalCustomerTxt.setText("Loyal Customer registered");
            customerIDField.clear();
            customerNameField.clear();
        }
        else{
            log.debug("Customer not found");
        }
    }

    @FXML
    public void nameFieldTyped() {
        if (nameField.getText().length() == 0) {
            suggestion.hide();
        } else {
            suggestion.setStyle("-fx-pref-width: " + nameField.getWidth() + "px;");
            suggestion.getItems().clear();
            suggestion.getItems().addAll(warehouse.fetchStockItems().stream().filter(item -> item.getName().toLowerCase().contains(nameField.getText().toLowerCase())).map(item -> {
                MenuItem menu = new MenuItem(item.getName());
                menu.setOnAction(e -> nameField.setText(menu.getText()));
                return menu;
            }).collect(Collectors.toList()));
            if(!(suggestion.getItems().size() == 1 && suggestion.getItems().get(0).getText().equals(nameField.getText())))
                suggestion.show(nameField, Side.BOTTOM, 0, 0);
        }
    }

    private Node loadControls(String fxml, Initializable controller) throws IOException {
        URL resource = SalesSystemUI.class.getResource(fxml);
        if (resource == null)
            log.error(fxml + " not found");
//            throw new IllegalArgumentException(fxml + " not found");

        FXMLLoader fxmlLoader = new FXMLLoader(resource);
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }


}
