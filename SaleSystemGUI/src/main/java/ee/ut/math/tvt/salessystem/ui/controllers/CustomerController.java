
package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class CustomerController implements Initializable {

    private final ShoppingCart shoppingCart;

    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private TitledPane shoppingCartPane;
    @FXML
    private TableColumn<SoldItem, SoldItem> discountCol;
    @FXML
    private TableColumn<SoldItem, SoldItem> totalCol;
    @FXML
    private Text totalTxt;
    @FXML
    private Text loyalCustomerTxt;

    public CustomerController(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        refreshSoldItems();

        discountCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));

        discountCol.setCellFactory(param -> new TableCell<SoldItem, SoldItem>() {
            @Override
            protected void updateItem(SoldItem item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.valueOf(item.calculateTotalDiscountProduct()));
                }
            }
        });

        totalCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()));

        totalCol.setCellFactory(param -> new TableCell<SoldItem, SoldItem>() {
            @Override
            protected void updateItem(SoldItem item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.valueOf(item.getTotalPrice()));
                }
            }
        });

    }

    public void refreshSoldItems() {
        totalTxt.setText(Float.toString(shoppingCart.calculatePrice()));
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        if(shoppingCart.getLoyalCustomer()!=null){
            loyalCustomerTxt.setText("Welcome back "+shoppingCart.getLoyalCustomer().getName()+"!");
        }
        else{
            loyalCustomerTxt.setText("Customer not registered");

        }
        purchaseTableView.refresh();
    }

}