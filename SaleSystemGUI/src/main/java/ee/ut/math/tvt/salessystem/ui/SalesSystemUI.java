package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.EmployeeLogic;
import ee.ut.math.tvt.salessystem.logic.History;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.controllers.*;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;

/**
 * Graphical user interface of the sales system.
 */
public class SalesSystemUI extends Application {

    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);

//    private final SalesSystemDAO dao;
    private final SalesSystemDAO daoH;
    private final ShoppingCart shoppingCart;
    private final Warehouse warehouse;
    private final History history;
    private final EmployeeLogic employee;

    public SalesSystemUI() {
        daoH = new InMemorySalesSystemDAO();
//        daoH = new HibernateSalesSystemDAO();
        shoppingCart = new ShoppingCart(daoH);
        warehouse = new Warehouse(daoH);
        history = new History(daoH);
        employee = new EmployeeLogic(daoH);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info("Salesystem GUI started");
        log.info("javafx version: " + System.getProperty("javafx.runtime.version"));
        primaryStage.setMinWidth(870);
        primaryStage.setMinHeight(600);

        Tab costumerTab = new Tab();
        costumerTab.setText("Customer View");
        costumerTab.setClosable(false);
        CustomerController customerContr = new CustomerController(shoppingCart);
        costumerTab.setContent(loadControls("CustomerTab.fxml", customerContr));




        Tab stockTab = new Tab();
        stockTab.setText("Warehouse");
        stockTab.setClosable(false);
        StockController stockContr = new StockController(warehouse);
        stockTab.setContent(loadControls("StockTab.fxml", stockContr));

        Tab purchaseTab = new Tab();
        purchaseTab.setText("Point-of-sale");
        purchaseTab.setClosable(false);
        purchaseTab.setContent(loadControls("PurchaseTab.fxml", new PurchaseController(warehouse, shoppingCart, customerContr, stockContr)));

        //Created team tab
//        Tab teamTab = new Tab();
//        teamTab.setText("Team");
//        teamTab.setClosable(false);
//        teamTab.setContent(loadControls("TeamTab.fxml", new TeamController("../src/main/resources/application.properties")));

        Tab historyTab = new Tab();
        historyTab.setText("History");
        historyTab.setClosable(false);
        historyTab.setContent(loadControls("HistoryTab.fxml", new HistoryController(history)));

        Tab employeeTab = new Tab();
        employeeTab.setText("Employees");
        employeeTab.setClosable(false);
        employeeTab.setContent(loadControls("EmployeeTab.fxml", new EmployeeController(employee)));

        Group root = new Group();
        Scene scene = new Scene(root, 1000, 600, Color.WHITE);
        try{
            scene.getStylesheets().add(getClass().getResource("DefaultTheme.css").toExternalForm());
        }
        catch (Exception e){
            log.error("Loading Stylesheet failed");
        }


        BorderPane borderPane = new BorderPane();
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        borderPane.setCenter(new TabPane(costumerTab,purchaseTab, stockTab, historyTab,employeeTab));//, teamTab));
        root.getChildren().add(borderPane);

        primaryStage.setTitle("Sales system");
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    @Override
    public void stop(){
        log.info("Salesystem GUI closed");
    }

    private Node loadControls(String fxml, Initializable controller) throws IOException {
        URL resource = getClass().getResource(fxml);
        if (resource == null)
            log.error(fxml + " not found");
//            throw new IllegalArgumentException(fxml + " not found");

        FXMLLoader fxmlLoader = new FXMLLoader(resource);
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch(args);
    }
}


