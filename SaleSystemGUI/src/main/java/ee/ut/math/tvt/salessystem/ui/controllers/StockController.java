package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.AlertDisplayer;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);

    private final Warehouse warehouse;

    private final AlertDisplayer alert = new AlertDisplayer();

    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField descriptionField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;

    @FXML
    private Button addItemButton;

    @FXML
    private Button changeQuantityButton;
    @FXML
    private Button changePriceButton;
    @FXML
    private Button removeButton;

    @FXML
    private TextField searchField;

    @FXML
    private TextField expiryDateItemId;

    @FXML
    private Button addExpiryDateButton;

    @FXML
    private DatePicker expiryDatePicker;

    @FXML
    private Button removeExpiryDateButton;

    @FXML
    private TableView<LocalDate> expiryDateTableView;

    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    @FXML
    private TableColumn<LocalDate, LocalDate> dateColumn ;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        log.debug("initializing StockController");

        //Method here for coloring the low quantity product, taken from
        // https://stackoverflow.com/questions/32119277/colouring-table-row-in-javafx
        warehouseTableView.setRowFactory(tv -> new TableRow<StockItem>() {
            @Override
            public void updateItem(StockItem item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setStyle("");
                }
                else{
                    setStyle("");
                    if (item.getQuantity() < 6 && item.getQuantity() > 2) {
                        setStyle("-fx-background-color: wheat;");
                    }
                    else if (item.getQuantity() < 3) {
                        setStyle("-fx-background-color: tomato;");
                    }
                    List<LocalDate> expDates = item.getExpiryDates();
                    LocalDate dateToday = LocalDate.now();
                    for(LocalDate expDate : expDates){
                        if(expDate.isBefore(dateToday)){
                            setStyle("-fx-background-color: yellow;");
                            break;
                        }
                    }
                }
            }
        });
        refreshStockItems();

        dateColumn.setCellValueFactory(
                cellData -> new SimpleObjectProperty<>(cellData.getValue()));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        dateColumn.setCellFactory(column -> new TableCell<>() {
            @Override
            protected void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(formatter.format(date));
                }
            }
        });

        this.expiryDateItemId.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    StockItem item = warehouse.fetchStockItem(Long.parseLong(expiryDateItemId.getText()));
                    setExpiryDateTableItems(item);
                }
            }
        });

        addItemButton.disableProperty().bind(
                Bindings.isEmpty(nameField.textProperty())
                        .or(Bindings.isEmpty(barCodeField.textProperty()))
                        .or(Bindings.isEmpty(quantityField.textProperty()))
                        .or(Bindings.isEmpty(priceField.textProperty()))
        );

        removeButton.disableProperty().bind(
                Bindings.isEmpty(nameField.textProperty())
                        .and(Bindings.isEmpty(barCodeField.textProperty()))
        );

        changePriceButton.disableProperty().bind(
                Bindings.isEmpty(nameField.textProperty())
                        .and(Bindings.isEmpty(barCodeField.textProperty()))
                        .or(Bindings.isEmpty(priceField.textProperty()))
        );

        changeQuantityButton.disableProperty().bind(
                Bindings.isEmpty(nameField.textProperty())
                        .and(Bindings.isEmpty(barCodeField.textProperty()))
                        .or(Bindings.isEmpty(quantityField.textProperty()))
        );

        addExpiryDateButton.disableProperty().bind(
                Bindings.isEmpty(expiryDateItemId.textProperty())
                        .or(expiryDatePicker.valueProperty().isNull())
        );

        removeExpiryDateButton.disableProperty().bind(
                Bindings.isEmpty(expiryDateItemId.textProperty())
                        .or(expiryDatePicker.valueProperty().isNull())
        );

        for (TextField textField : new TextField[]{barCodeField, quantityField, expiryDateItemId}) {
            textField.textProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue.matches("\\d*")) {
                    textField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });
        }

        priceField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*\\.?\\d*")) {
                newValue = newValue.replaceAll("[^\\d.]", "");
                priceField.setText(newValue);
            }
            if(!newValue.matches("\\d*\\.?\\d*")){
                String[] values = newValue.split("\\.", -1);
                StringBuilder sb = new StringBuilder(values[0]);
                if (values.length > 1) {
                    sb.append(".").append(values[1]);
                    for (int i = 2; i < values.length; i++) {
                        sb.append(values[i]);
                    }
                }
                priceField.setText(sb.toString());
            }
        });

        //Timer to refresh stockitem list
        //https://stackoverflow.com/questions/11416242/how-to-repeatedly-call-a-function-after-a-certain-amount-of-time
/*        Timer t = new Timer();
        t.scheduleAtFixedRate(
                new TimerTask()
                {
                    public void run()
                    {
                        refreshStockItems();
                    }
                },
                0,      // run first occurrence immediatetly
                200); // run every second
*/
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }



    private StockItem getStockItemFromFieldInput() throws NumberFormatException, SalesSystemException {
        log.debug("Searching item from Stock using input data");
        StockItem barcodeStockItem;

        StockItem nameStockItem = warehouse.fetchStockItem(nameField.getText());
        String txtBarCode = barCodeField.getText();
        if (!txtBarCode.equals("")) {
            try {
                barcodeStockItem = warehouse.fetchStockItem(Long.parseLong(txtBarCode));
                if (!nameField.getText().equals("") && barcodeStockItem != nameStockItem){
                    log.debug("Input ID does not match with name");
                    throw new SalesSystemException("Id does not match with name.");
                }
                return barcodeStockItem;
            }catch (NumberFormatException e){
                log.debug("Barcode is in wrong format");
                throw new NumberFormatException("Barcode is in wrong format.");
            }
        }

        return nameStockItem;
    }

    @FXML
    public void changeQuantityButtonClicked() {


        try {
            StockItem inStockItem = getStockItemFromFieldInput();
            log.debug("Changing quantity of "+inStockItem.getName()+" to "+quantityField.getText());
            warehouse.changeItemQuantity(inStockItem, Integer.parseInt(quantityField.getText()));
            resetEntryFields();
            refreshStockItems();
        }
        catch (SalesSystemException | NumberFormatException e){
            log.debug("Exception occured when changing quantity: "+e);
            alert.display("Error", e.getMessage(), "");
        }
    }

    @FXML
    public void changePriceButtonClicked() {


        try {
            StockItem inStockItem = getStockItemFromFieldInput();
            log.debug("Changing price of "+inStockItem.getName());
            warehouse.changeItemPrice(inStockItem, Double.parseDouble(priceField.getText()));
            resetEntryFields();
            refreshStockItems();
        }
        catch (SalesSystemException | NumberFormatException e){
            log.debug("Exception occured when changing price: "+e);
            alert.display("Error", e.getMessage(), "");
        }
    }

    @FXML
    public void removeButtonClicked() {


        try {
            StockItem inStockItem = getStockItemFromFieldInput();
            log.debug("removing "+inStockItem.getName() +" from Warehouse");
            warehouse.removeItem(inStockItem);
            resetEntryFields();
            refreshStockItems();
        }
        catch (SalesSystemException | NumberFormatException e){
            log.debug("Exception occurred when removing item: "+e);
            alert.display("Error", e.getMessage(), "");
        }
    }

    @FXML
    public void addItemButtonClicked() {

        try {
            long id = Long.parseLong(barCodeField.getText());
            int quantity = Integer.parseInt(quantityField.getText());
            double price = Double.parseDouble(priceField.getText());
            String name = nameField.getText();
            log.debug("Adding "+name+ " to warehouse");
            try {
                String description = descriptionField.getText();
                warehouse.addItem(new StockItem(id, name, description, price, quantity));
                refreshStockItems();
                resetEntryFields();
            } catch (SalesSystemException e) {
                log.debug("Exception occurred when adding item: "+e.getMessage());
                alert.display("Error", e.getMessage(), "");
            }
        }catch (NumberFormatException e){
            log.debug("Exception occurred when adding item: Incorrect price format");
            alert.display("Error", "Incorrect price format.", e.getLocalizedMessage());
        }
    }

    @FXML
    public void tableRowClicked() {
        StockItem chosenItem = warehouseTableView.getSelectionModel().getSelectedItem();
        if (chosenItem != null) {
            log.debug(chosenItem.getName() + "selected in table");
            fillEntryFields(chosenItem);
            setExpiryDateTableItems(chosenItem);
        }

    }

    private void setExpiryDateTableItems(StockItem chosenItem){
        log.debug("Setting expiry date table items");
        if(chosenItem != null) {
            expiryDateItemId.setText(chosenItem.getId().toString());
            if (chosenItem.getExpiryDates() != null && !chosenItem.getExpiryDates().isEmpty()) {
                expiryDateTableView.setItems(FXCollections.observableList(chosenItem.getExpiryDates()));
            } else expiryDateTableView.setItems(null);
            expiryDatePicker.setValue(null);
        }
        else expiryDateTableView.setItems(null);
        refreshExpiryDates();
    }

    @FXML
    public void expiryDateTableRowClicked() {

        LocalDate chosenDate = expiryDateTableView.getSelectionModel().getSelectedItem();
        log.debug(chosenDate.toString() + "selected in table");

        expiryDatePicker.setValue(chosenDate);
    }

    @FXML
    public void removeExpiryDateButtonClicked() {


        try {
            log.debug("removing Expiry Date from "+expiryDateItemId.getText());
            LocalDate date = expiryDatePicker.getValue();
            long id = Long.parseLong(expiryDateItemId.getText());
            warehouse.removeExpiryDate(id, date);
            expiryDateTableView.setItems(FXCollections.observableList(warehouse.fetchStockItem(id).getExpiryDates()));
        }
        catch (NumberFormatException e){
            alert.display("Error", "Barcode in wrong format.", "");
            log.debug("Exception occurred when removing Expiry Date from item: Incorrect barcode format");
        }
        catch (SalesSystemException e){
            alert.display("Error", e.getMessage(), "");
            log.debug("Exception occurred when removing Expiry Date from item:"+ e.getMessage());
        }

        refreshExpiryDates();
        expiryDatePicker.setValue(null);

    }

    @FXML
    public void addExpiryDateButtonClicked() {

        try {
            log.debug("Adding Expiry Date to "+expiryDateItemId.getText());

            LocalDate date = expiryDatePicker.getValue();
            long id = Long.parseLong(expiryDateItemId.getText());
            warehouse.addExpiryDate(id,date);
            expiryDateTableView.setItems(FXCollections.observableList(warehouse.fetchStockItem(id).getExpiryDates()));
        }
        catch (NumberFormatException e){
            alert.display("Error", "Barcode in wrong format.", "");
            log.debug("Exception occurred when adding expiry date: Barcode in wrong format");
        }
        catch (SalesSystemException e){
            alert.display("Error", e.getMessage(), "");
            log.debug("Exception occurred when adding expiry date: "+e.getMessage());
        }

        refreshExpiryDates();
        expiryDatePicker.setValue(null);
    }


    @FXML
    public void searchFieldTyped(){
        searchItems(searchField.getText());
    }

    private void fillEntryFields(StockItem item) {
        barCodeField.setText(Long.toString(item.getId()));
        nameField.setText(item.getName());
        quantityField.setText(Integer.toString(item.getQuantity()));
        priceField.setText(Double.toString(item.getPrice()));
        descriptionField.setText(item.getDescription());
    }

    private void resetEntryFields() {
        barCodeField.setText("");
        nameField.setText("");
        quantityField.setText("");
        priceField.setText("");
        descriptionField.setText("");
    }

    private void searchItems(String filter) {
        warehouseTableView.setItems(FXCollections.observableList(warehouse.fetchStockItems(filter)));
    }

    public void refreshStockItems() {
        log.debug("Refreshing StockItems table");
        if (searchField.getText().equals("")) {
            warehouseTableView.setItems(FXCollections.observableList(warehouse.fetchStockItems()));
        }
        else searchItems(searchField.getText());
        warehouseTableView.refresh();
    }

    private void refreshExpiryDates()
    {
       log.debug("Refreshing expiry dates");
        expiryDateTableView.refresh();
    }


}
