package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.logic.Payment;
import ee.ut.math.tvt.salessystem.ui.AlertDisplayer;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PaymentController implements Initializable {

    Payment payment;
    private float returnCash;
    private boolean printReceipt = false;
    private final AlertDisplayer alert = new AlertDisplayer();
    private String endPaneTxt;

////////Cash or Credit
    @FXML
    private TitledPane paymentPane;

    @FXML
    private Button chooseCash;

    @FXML
    private Button chooseCredit;

    @FXML
    private Button cancelPayment1Button;
/////////Cash Pane
    @FXML
    private TitledPane cashPane;

    @FXML
    private Button submitPaymentCash;

    @FXML
    private TextField priceFieldCash;

    @FXML
    private TextField givenCashField;

    @FXML
    private Button cancelPaymentCashButton;
////////////Credit pane
    @FXML
    private TitledPane creditPane;

    @FXML
    private TextField priceFieldCredit;

    @FXML
    private TextField pinFieldCredit;

    @FXML
    private Button cancelPaymentCreditButton;
///////// End pane
    @FXML
    private TitledPane endPane;

    @FXML
    private Text commentLeft;
    @FXML
    private Text priceTxt;
    @FXML
    private Text MethodTxt;
    @FXML
    private Text commentRight;
    @FXML
    private ToggleButton printReceiptButton;
    @FXML
    private Button closePaymentButton;

    public PaymentController(Payment payment){
        this.payment = payment;
    }

    public void initialize(URL location, ResourceBundle resources) {
        paymentPane.setVisible(true);
        cashPane.setVisible(false);
        creditPane.setVisible(false);
        endPane.setVisible(false);

    }
///////PaymentPane
    @FXML
    public void chooseCashClicked(){
        priceFieldCash.setText(Float.toString(payment.getAmountToPay()));
        priceTxt.setText(Float.toString(payment.getAmountToPay()));
        paymentPane.setVisible(false);
        cashPane.setVisible(true);
        payment.setCashPayment(true);
        MethodTxt.setText("Cash");
    }

    @FXML
    public void chooseCreditClicked(){
        priceFieldCredit.setText(Float.toString(payment.getAmountToPay()));
        priceTxt.setText(Float.toString(payment.getAmountToPay()));
        paymentPane.setVisible(false);
        creditPane.setVisible(true);
        payment.setCashPayment(false);
        MethodTxt.setText("Credit Card");

    }
    @FXML
    public void cancelPaymentButtonClicked(){
        Stage stage = (Stage) cancelPayment1Button.getScene().getWindow();
        stage.close();
    }
//////////Cash Payment
    @FXML
    public void submitPaymentCashClicked(){
        try{
            float givenCash = Float.parseFloat(givenCashField.getText());
            returnCash = payment.calculateReturn(givenCash);
            if(returnCash>=0){
                cashPane.setVisible(false);
                endPaneTxt = "Return "+Float.toString(returnCash)+" to costumer";
                commentLeft.setText("Received: "+Float.toString(givenCash));
                commentRight.setText("Returned: "+Float.toString(returnCash));
                endPane.setVisible(true);
            }
            else{
                alert.display("Cash Payment", "Less money received than the price of Shopping cart",
                        "Check Your entry");
            }
        }
        catch (Exception e){
            alert.display("Cash Payment", "Cannot parse received cash!",
                    "Check Your entry");
        }
    }

    //////Credit payment
    @FXML
    public void submitPaymentCreditClicked(){
        //int enteredPin = Integer.parseInt(pinFieldCredit.getText());
        String enteredPin = pinFieldCredit.getText();
        if(payment.checkPIN(enteredPin)){
            creditPane.setVisible(false);
            commentLeft.setText("");
            commentRight.setText("PIN verified");
            endPane.setVisible(true);
        }
        else{
            alert.display("Credit Payment", "PIN incorrect", "Check Your entry");
        }
    }
    @FXML
    public void printReceiptButtonClicked(){
        printReceipt=!printReceipt;
    }

    public boolean getPrintReceipt(){
        return printReceipt;
    }
}
