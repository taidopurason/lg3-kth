package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.History;
import ee.ut.math.tvt.salessystem.logic.Payment;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.ui.AlertDisplayer;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);

//    private final SalesSystemDAO dao;

    private final AlertDisplayer alert = new AlertDisplayer();

    private final History history;

    @FXML
    private TableView<Purchase> historyTableView;
    @FXML
    private TableView<SoldItem> historyDetailsTableView;
    @FXML
    private TextField historySearchTextField;
    @FXML
    private Button historyGenerateChartButton;
    @FXML
    private DatePicker historyStartDateDatePicker;
    @FXML
    private DatePicker historyEndDateDatePicker;
    @FXML
    private LineChart<String, Number> historyLineChart;
    @FXML
    private Accordion historyLeftAccordion;
    @FXML
    private Accordion historyRightAccordion;
    @FXML
    private TitledPane historyPurchasesPane;
    @FXML
    private TitledPane historyPurchaseDetailsPane;
    @FXML
    private TitledPane historyItemDetailsPane;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField originalPriceTextField;
    @FXML
    private DatePicker tableStartDatePicker;
    @FXML
    private DatePicker tableEndDatePicker;
    @FXML
    private Button tableShowAllButton;
    @FXML
    private Button tableShowLast10Button;
    @FXML
    private Button tableShowRangeButton;

    public HistoryController (History history) {
//        this.dao = dao;
        this.history = history;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //fill history table with items.
        historyTableView.setItems(FXCollections.observableList(history.getPurchases()));

        historyTableView.setOnMouseClicked((e -> {
            if (historyTableView.getSelectionModel().getSelectedItem() != null) {
                historyDetailsTableView.setItems(FXCollections.observableList(historyTableView.getSelectionModel().getSelectedItem().getSoldItems()));
                historyDetailsTableView.refresh();
                historyRightAccordion.setExpandedPane(historyPurchaseDetailsPane);

                log.debug("Clicked on History Purchases table, updating data in Purchase Details table.");
            }
        }));

        historyDetailsTableView.setOnMouseClicked((e -> {
            if (historyDetailsTableView.getSelectionModel().getSelectedItem() != null) {
                SoldItem selectedItem = historyDetailsTableView.getSelectionModel().getSelectedItem();
                nameTextField.setText(selectedItem.getName());
                quantityTextField.setText("" + selectedItem.getQuantity());
                priceTextField.setText("" + selectedItem.getPrice());
                originalPriceTextField.setText("" + selectedItem.getOriginalPrice());
                historyLeftAccordion.setExpandedPane(historyItemDetailsPane);

                log.debug("Clicked on Purchase Details table, updating info in Entry fields in item details pane.");
            }
        }));

        //automatically fill the end and start dates. (to graph)
        historyEndDateDatePicker.setValue(LocalDate.now());
        historyStartDateDatePicker.setValue(LocalDate.now().minus(7, ChronoUnit.DAYS));

        //automatically fill the end and start dates. (to purchases table.)
        tableEndDatePicker.setValue(LocalDate.now());
        tableStartDatePicker.setValue(LocalDate.now().minus(7,ChronoUnit.DAYS));

        //since scenebuilder automatically deletes the expandedPane="$..." option from accordions, default expanded panes are being written inline for now
        //The expandedpane attributes were added inline to FXML so that it meets the Lab6 requirements for now.
        //If editing by scenebuilder then these two lines of code should be uncommented.
        //DELETEME
        //historyLeftAccordion.setExpandedPane(historyPurchasesPane);
        //historyRightAccordion.setExpandedPane(historyPurchaseDetailsPane);

        log.info("initializing HistoryController");
    }

    public void historyRefreshButtonClicked () {
        refreshPurchases();
    }

    public void historyGenerateChartButtonClicked () {
        generateChart();
    }

    public void historySearchTextFieldTyped () {
        refreshPurchases();
    }

    public void cancelButtonClicked () {
        historyLeftAccordion.setExpandedPane(historyPurchasesPane);
    }

    public void tableShowRangeButtonClicked () {
        Date startDate;
        Date endDate;

        try {
            startDate = history.localDatetoDate(tableStartDatePicker.getValue());//.getTime();
            endDate = history.localDatetoDate(tableEndDatePicker.getValue());//.getTime();
        } catch (Exception e) {
            alert.display("Error", "Error parsing dates from datepickers!", "");
            return;
        }

        historyTableView.setItems(FXCollections.observableList(history.getPurchasesBetweenDates(startDate, endDate)));
        historyTableView.refresh();

    }

    public void tableShowAllButtonClicked () {
        historyTableView.setItems(FXCollections.observableList(history.getPurchases()));
        historyTableView.refresh();
        log.debug("Showing all in Purchase History table.");
    }

    public void tableShowLast10ButtonClicked () {
        historyTableView.setItems(FXCollections.observableList(history.getLast10Purchases()));
        historyTableView.refresh();
        log.debug("Showing last 10 in Purchase History table.");
    }

    public void saveButtonClicked () {
        SoldItem selected = historyDetailsTableView.getSelectionModel().getSelectedItem();
        String name;
        int quantity;
        double price;
        double originalPrice;
        try {
            name = nameTextField.getText();
            quantity = Integer.parseInt(quantityTextField.getText());
            price = Double.parseDouble(priceTextField.getText());
            originalPrice = Double.parseDouble(originalPriceTextField.getText());
        } catch (NumberFormatException e) {
            alert.display("Error", "Exception occured when parsing numbers.", "");
            log.debug("Exception occurred when parsing TextFields to edit SoldItem.");
            return; //exit out of the button click
        }
        //updates solditem details.
        selected.setName(name);
        selected.setQuantity(quantity);
        selected.setPrice(price);
        selected.setOriginalPrice(originalPrice);
        historyDetailsTableView.refresh();
        //updates the purchases sum.
        historyTableView.getSelectionModel().getSelectedItem().recalculateSum();
        historyTableView.refresh();
        historyLeftAccordion.setExpandedPane(historyPurchasesPane);

        log.debug("Successfully edited SoldItem data in Purchase.");
    }

    private void refreshPurchases () {
        log.debug("Refreshing Purchases table");
        if (historySearchTextField.getText().equals("")) {
            historyTableView.setItems(FXCollections.observableList(history.getPurchases()));
        } else {
            searchItems(historySearchTextField.getText());
        }
        historyTableView.refresh();
    }

    private void generateChart () {

        //clear the chart for a clearer view.
        historyLineChart.getData().clear();

        //Adding the calculated clock time difference to startDate and endDate

        XYChart.Series<String, Number> series = new XYChart.Series();
        LinkedHashMap<String, Number> rows = history.generateFigure(historyStartDateDatePicker.getValue(),
                historyEndDateDatePicker.getValue());
        //Add data to list
        ArrayList<String> datesInRange = new ArrayList(rows.keySet());
        for(int i=0;i<datesInRange.size();i++){
            series.getData().add(new XYChart.Data<String, Number>(datesInRange.get(i), rows.get(datesInRange.get(i))));
        }

        historyLineChart.getData().add(series);
        historyLineChart.getXAxis().setLabel("Date");
        historyLineChart.getYAxis().setLabel("Income on given date [Euro]");

        log.debug("Chart successfully generated");
    }

    private void searchItems(String filter) {
        historyTableView.setItems(FXCollections.observableArrayList(history.getItems(filter)));
    }
}
