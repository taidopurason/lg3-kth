package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;
import ee.ut.math.tvt.salessystem.logic.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final ee.ut.math.tvt.salessystem.logic.CLILogic CLILogic;
    private final ShoppingCart cart;
    public Payment payment;
    private Warehouse warehouse;
    private EmployeeLogic employeeLog;
    private History history;

    public ConsoleUI(SalesSystemDAO dao) {
        this.cart = new ShoppingCart(dao);
        this.warehouse = new Warehouse(dao);
        this.employeeLog = new EmployeeLogic(dao);
        this.history = new History(dao);
        this.CLILogic = new CLILogic(warehouse, cart, employeeLog, history);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao =  new InMemorySalesSystemDAO(); // new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        log.info("Salesystem CLI started");
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        log.debug("Showing current Stock");
        List<StockItem> stockItems = warehouse.fetchStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    //Method for adding item to Warehouse
    private void addStock() {
        log.debug("Adding product to Stock");
        System.out.println("#####Adding product to stock #####");
        System.out.println("##################################");

        try {
            CLILogic.addStock();

        } catch (Exception e) {
            log.debug("Problem parsing input data");
            System.out.println("Something went wrong, check entered data");
        }
    }

    //Method for changing the quantity and price of StockItem
    private void modifyStock() {

        log.debug("Modifying product in Stock");
        System.out.println("### Modifying product in stock ###");
        System.out.println("##################################");
        try {
            CLILogic.modifyStock();
        } catch (Exception e) {
            log.debug("Problem parsing input data");
            System.out.println("Something went wrong, check entered data");
        }
    }

    private void showCart() {
        log.debug("Showing Cart");
        System.out.println("-------------------------");
        System.out.println(String.format("%20s %5s %20s %5s %10s %5s %15s %5s %10s", "Product name", "|",
                "Item price(Eur)", "|",  "Quantity", "|", "Item Discount", "|", "Total"));
        System.out.println("-------------------------------------------------------------------------------------------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(String.format("%20s %5s %20s %5s %10s %5s %15s %5s %10s", si.getName(), "|",
                    si.getOriginalPrice(),"|", si.getQuantity(),"|", si.calculateDiscount(),"|", si.getTotalPrice()));
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        else{
            System.out.println("-------------------------------------------------------------------------------------------------------------");
            System.out.println("TOTAL PRICE OF CART: "+ cart.calculatePrice()+" Euros");
        }
        System.out.println("-------------------------");
    }

    private void removeFromCart() {
        log.debug("Removing from cart");
        System.out.println("##### Removing product from cart #####");
        System.out.println("#######################################");
        try{
            CLILogic.removeItemFromCart();
        }
        catch(IOException e){
            log.debug("Problem with IO");
        }
    }

    //Method for showing the employees
    private void showEmployees() {
        log.debug("Showing Employees");
        System.out.println();
        System.out.println("===========================");
        System.out.println("=     Employees' Data     =");
        System.out.println("===========================");

        List<Employee> employeeList = CLILogic.getEmployeeList();

        for (Employee worker : employeeList) {
            System.out.println("-------------------------");
            System.out.println(worker.toString());
            System.out.println(worker.showData());
        }

    }

    private void showHistory(){
        log.debug("Showing History");
        List<Purchase> historyList = history.getPurchases();
        System.out.println();
        System.out.println("===========================");
        System.out.println("=     Purchase History    =");
        System.out.println("===========================");


        System.out.println(String.format("%5s %2s %7s %2s %15s ", "ID", "|",
                "Method", "|", "Date"));
        for (Purchase purchase : historyList) {
            System.out.println(purchase.printData());
        }
    }

    private void printHistoryPurchase(String filter){
        Purchase selected = history.getItems(filter).get(0);
        System.out.println(selected.printReceipt());
    }

    //Method for showing the team information
    private void showTeam() {
        log.debug("Showing Team");
        System.out.println();
        System.out.println("===========================");
        System.out.println("=        Team Info        =");
        System.out.println("===========================");
        System.out.println(CLILogic.getTeamAsString());
        System.out.println("-------------------------");
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("k\t\tAdd product to warehouse");
        System.out.println("m\t\tModify product quantity and price in warehouse");
        System.out.println("-------------------------");
        System.out.println("c\t\tShow cart contents");
        System.out.println("b\t\tPrint receipt of last purchase");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("r\t\tRemove item from Cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("l\t\tCheck Loyal Customer for discounts");
        System.out.println("b\t\tPrint receipt of last purchase");
        System.out.println("-------------------------");
        System.out.println("x\t\tShow Purchase history");
        System.out.println("z ID \t Show Purchase content with given ID");
        System.out.println("-------------------------");
        System.out.println("e\t\tShow the Employees' details");
        System.out.println("t\t\tShow the Team information");
        System.out.println("-------------------------");
        System.out.println("q\t\tExit the application");
        System.out.println("-------------------------");
    }

    private void printLastReceipt(){
        if(cart.getLastPurchase()!=null){
            System.out.println(cart.getLastPurchase().printReceipt());
        }
    }

    //Verify if customer is in loyality program
    private void loyalCustomerProcedure(){
        log.debug("Loyal Customer procedure started");
        //Ask customer data
            try{
                CLILogic.loyalCostumerCheck(cart);
            }
            catch(IOException e){
                log.error("IOException in loyalCustomerProcedure");
            }
        }

    private void processCommand(String command) {
        String[] c = command.split(" ");
        log.debug("Processing command: " + c[0]);
        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("x"))
            showHistory();
        else if(c.length==2 && c[0].equals("z")){
            printHistoryPurchase(c[1]);
        }
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("r"))
            removeFromCart();
            //Added Team
        else if (c[0].equals("t"))
            showTeam();
            //Added Employee
        else if (c[0].equals("e"))
            showEmployees();
            //Add item to Warehouse
        else if (c[0].equals("k"))
            addStock();
        else if(c[0].equals("b")){
            printLastReceipt();
        }
            //Modify item in Warehouse
        else if (c[0].equals("m"))
            modifyStock();
        else if(c[0].equals("l"))
            loyalCustomerProcedure();
        else if (c[0].equals("p")) { //defaulting to cash payment, CLI needs to have option to select between the two payment modes. (Credit/Cash)
            payment = new Payment(cart.calculatePrice());
            if( CLILogic.paymentProcedure(payment)){
                System.out.println("Transaction success");
                cart.submitCurrentPurchase(payment.cashOrCredit());}
                log.debug("Transaction completed successfully");
            }
        else if (c[0].equals("r")) {
                cart.cancelCurrentPurchase();
            }
        else if (c[0].equals("a") && c.length == 3) {
                try {
                    long idx = Long.parseLong(c[1]);
                    int amount = Integer.parseInt(c[2]);
                    StockItem item = warehouse.fetchStockItem(idx);
                    if(item.getQuantity()<amount){
                        System.out.println("Can't add so large quantity of product into cart");
                        System.out.println("There is not enough product in stock");
                    }
                    else if (item != null) {
                        cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                    } else {
                        System.out.println("no stock item with id " + idx);
                    }
                } catch (SalesSystemException | NoSuchElementException e) {
                    log.error(e.getMessage(), e);
                }
            }
        else {
                System.out.println("unknown command");
            }
        }

    }

