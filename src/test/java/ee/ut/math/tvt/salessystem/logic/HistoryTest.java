package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class HistoryTest {
    private History history;
    private MockDAO dao;

    private class MockDAO extends InMemorySalesSystemDAO {
        private final List<String> methodsCalled = new ArrayList<>();
        private final List<StockItem> stockItemList;

        public MockDAO(List<StockItem> stockItemList) {
            this.stockItemList = stockItemList;

        }

        public MockDAO() {
            List<StockItem> items = new ArrayList<StockItem>();
            items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 100));
            items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 100));
            items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 100));
            items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            this.stockItemList = items;

            SoldItem itemS = new SoldItem(items.get(0), 1);
            SoldItem itemS2 = new SoldItem(items.get(1), 4);
            SoldItem itemS3 = new SoldItem(items.get(2), 5);
            SoldItem itemS4 = new SoldItem(items.get(3), 5);

            //Create 12 Purchases
            for(int i=0; i < 4; i++){

                List<SoldItem> soldItems = new ArrayList<SoldItem>();
                soldItems.add(new SoldItem(items.get(0), 2));
                soldItems.add(new SoldItem(items.get(3), 1));
                Purchase purchase1 = new Purchase(1L, soldItems, "Cash");

                List<SoldItem> soldItems2 = new ArrayList<SoldItem>();
                soldItems2.add(new SoldItem(items.get(1), 5));
                soldItems2.add(new SoldItem(items.get(0), 1));
                soldItems2.add(new SoldItem(items.get(3), 4));

                Purchase purchase2 = new Purchase(2L, soldItems2, "Credit");
                List<SoldItem> soldItems3 = new ArrayList<SoldItem>();
                soldItems3.add(new SoldItem(items.get(0), 1));

                Purchase purchase3 = new Purchase(3L, soldItems3, "Cash");

                super.savePurchase(purchase1);
                super.savePurchase(purchase2);
                super.savePurchase(purchase3);
            }
        }

        public void resetMethods() {
            methodsCalled.clear();
        }

        public void resetStockItems(boolean addDefaultItems) {
            stockItemList.clear();
            if (addDefaultItems) {
                stockItemList.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 100));
                stockItemList.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 100));
                stockItemList.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 100));
                stockItemList.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            }
        }

        public List<String> getMethodsCalled() {
            return methodsCalled;
        }

        @Override
        public void beginTransaction() {
            methodsCalled.add("beginTransaction");
        }

        @Override
        public void rollbackTransaction() {
            methodsCalled.add("rollbackTransaction");
        }

    }

    @Before
    public void initializeDAOandWarehouse(){
        dao = new HistoryTest.MockDAO();
        history = new History(dao);
    }

    @Test
    public void testGetAllPurchases(){
        HistoryTest.MockDAO dao = new HistoryTest.MockDAO();
        List<Purchase> purchases = dao.getPurchaseList();
        assertNotNull("Did not receive any purchases from History", purchases);
    }

    @Test
    public void testGettingLast10Purchases(){
        HistoryTest.MockDAO dao = new HistoryTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        List<Purchase> last10 = dao.getLast10Purchases();
        assertEquals("Did not get right number of Purchases with Last 10",10,last10.size());
    }

    @Test
    public void testGetItemsByFilter(){
        HistoryTest.MockDAO dao = new HistoryTest.MockDAO();
        List<Purchase> purchases = history.getItems("1");
        for(int i=0;i<purchases.size();i++){
            if(purchases.get(i).getId()!=1L){
                fail("Not correct items received using filter");
            }
        }
    }

    @Test
    public void testGetItemInRange(){
        Date startDate = new Date(System.currentTimeMillis()-1000000);
        Date endDate = new Date(System.currentTimeMillis()+1000);
        List<Purchase> inRangePurchases = history.getPurchasesBetweenDates(startDate, endDate);
        for (Purchase purchase : inRangePurchases){
            if(purchase.getDate().getTime()>=endDate.getTime() || purchase.getDate().getTime()<=startDate.getTime()){
                fail("Purchases in range not correct");
            }
        }

    }

}

