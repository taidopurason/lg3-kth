package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WarehouseTest {
    private MockDAO dao;
    private Warehouse warehouse;

    private class MockDAO extends InMemorySalesSystemDAO {
        private final List<String> methodsCalled = new ArrayList<>();
        private final List<StockItem> stockItemList;

        public MockDAO(List<StockItem> stockItemList) {
            this.stockItemList = stockItemList;
        }

        public MockDAO() {
            List<StockItem> items = new ArrayList<StockItem>();
            items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
            items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
            items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
            items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            this.stockItemList = items;
        }

        public void resetMethods(){
            methodsCalled.clear();
        }

        public void resetStockItems(boolean addDefaultItems){
            stockItemList.clear();
            if (addDefaultItems){
                stockItemList.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
                stockItemList.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
                stockItemList.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
                stockItemList.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            }
        }

        public List<String> getMethodsCalled() {
            return methodsCalled;
        }

        @Override
        public void beginTransaction() {
            methodsCalled.add("beginTransaction");
        }

        @Override
        public void rollbackTransaction() {
            methodsCalled.add("rollbackTransaction");
        }

        @Override
        public void saveStockItem(StockItem item) {
            methodsCalled.add("saveStockItem");
            super.saveStockItem(item);
        }

        @Override
        public void removeStockItem(StockItem item) {
            methodsCalled.add("removeStockItem");
            super.removeStockItem(item);
        }

        @Override
        public void commitTransaction() {
            methodsCalled.add("commitTransaction");
        }
    }

    @Before
    public void initializeDAOandWarehouse(){
        dao = new MockDAO();
        warehouse = new Warehouse(dao);
    }


    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);
        warehouse.addItem(item);
        List<String> methodsCalled = dao.getMethodsCalled();

        String testCase = "when adding a new item";
        assertFalse("rollbackTransaction must not be called " + testCase + ".", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction(), saveStockItem(), commitTransaction() must each be called once when adding a new item " + testCase + ".", 3, methodsCalled.size());
        assertEquals("beginTransaction() must be called first " + testCase + ".", "beginTransaction", methodsCalled.get(0));
        assertEquals("Item must be saved before commiting by calling saveStockItem() " + testCase + ".", "saveStockItem", methodsCalled.get(1));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing " + testCase + ".", "commitTransaction", methodsCalled.get(2));

        dao.resetMethods();
        warehouse.addItem(item);
        methodsCalled = dao.getMethodsCalled();

        testCase = "when adding an existing item";
        assertFalse("rollbackTransaction must not be called " + testCase + ".", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction(), commitTransaction() must each be called once when adding a new item " + testCase + ".", 2, methodsCalled.size());
        assertFalse("saveStockItem() must not be called " + testCase + ".", methodsCalled.contains("saveStockItem"));
        assertEquals("beginTransaction() must be called first " + testCase + ".", "beginTransaction", methodsCalled.get(0));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing " + testCase + ".", "commitTransaction", methodsCalled.get(1));
    }

    @Test
    public void testAddingNewItem() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);

        warehouse.addItem(item);

        StockItem itemFromTheDatabase = dao.findStockItem(5);
        assertNotNull("Item must be present in the database after saving it.", itemFromTheDatabase);
        assertEquals("IDs must match.", item.getId(), itemFromTheDatabase.getId());
        assertEquals("Names must match.", item.getName(), itemFromTheDatabase.getName());
        assertEquals("Descriptions must match.", item.getDescription(), itemFromTheDatabase.getDescription());
        assertEquals("Prices must match.", item.getPrice(), itemFromTheDatabase.getPrice(), 0.0000001);
        assertEquals("Quantities must match.", item.getQuantity(), itemFromTheDatabase.getQuantity());
    }

    @Test
    public void testAddingExistingItem() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);
        StockItem item2 = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 213);

        warehouse.addItem(item);
        dao.resetMethods();
        warehouse.addItem(item2);

        StockItem itemFromTheDatabase = dao.findStockItem(5);
        assertNotNull("Item must be present in the database after saving it.", itemFromTheDatabase);
        assertEquals("IDs must match.", item.getId(), itemFromTheDatabase.getId());
        assertEquals("Names must match.", item.getName(), itemFromTheDatabase.getName());
        assertEquals("Descriptions must match.", item.getDescription(), itemFromTheDatabase.getDescription());
        assertEquals("Prices must match.", item.getPrice(), itemFromTheDatabase.getPrice(), 0.0000001);
        assertEquals("Quantity of new item must be added.", 313, itemFromTheDatabase.getQuantity());
        assertFalse("saveStockItem() must not be called ", dao.getMethodsCalled().contains("saveStockItem"));
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, -2);

        try {
            warehouse.addItem(item);
            fail("addItem() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertFalse("saveStockItem() must not be called.", dao.getMethodsCalled().contains("saveStockItem"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testAddingItemWithNegativePrice() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", -3.0, 1);

        try {
            warehouse.addItem(item);
            fail("addItem() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertFalse("saveStockItem() must not be called.", dao.getMethodsCalled().contains("saveStockItem"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingItemBeginsAndCommitsTransaction() {
        StockItem item = dao.findStockItem(4);
        warehouse.removeItem(item);
        List<String> methodsCalled = dao.getMethodsCalled();

        assertFalse("rollbackTransaction must not be called.", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction(), removeStockItem() and commitTransaction() must each be called once.", 3, methodsCalled.size());
        assertEquals("beginTransaction() must be called first.", "beginTransaction", methodsCalled.get(0));
        assertEquals("Item must be removed before commiting by calling removeStockItem().", "removeStockItem", methodsCalled.get(1));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing.", "commitTransaction", methodsCalled.get(2));
    }

    @Test
    public void testRemovingExistingItem() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);

        warehouse.addItem(item);
        dao.resetMethods();
        warehouse.removeItem(item);

        assertNull("Item must not be present in the database after saving it.", dao.findStockItem(5));
    }

    @Test
    public void testRemovingNonExistingItem() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 2);

        try {
            warehouse.removeItem(item);
            fail("removeItem() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertFalse("removeStockItem() must not be called.", dao.getMethodsCalled().contains("removeStockItem"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingNullItem() {
        StockItem item = null;

        try {
            warehouse.removeItem(item);
            fail("removeItem() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertFalse("removeStockItem() must not be called.", dao.getMethodsCalled().contains("removeStockItem"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemQuantityBeginsAndCommitsTransaction() {
        MockDAO dao = new MockDAO();
        Warehouse warehouse = new Warehouse(dao);
        StockItem item = dao.findStockItem(4);
        warehouse.changeItemQuantity(item, 10);
        List<String> methodsCalled = dao.getMethodsCalled();

        assertFalse("saveStockItem() must not be called.", dao.getMethodsCalled().contains("saveStockItem"));
        assertFalse("rollbackTransaction must not be called.", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction() and commitTransaction() must each be called once.", 2, methodsCalled.size());
        assertEquals("beginTransaction() must be called first.", "beginTransaction", methodsCalled.get(0));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing.", "commitTransaction", methodsCalled.get(1));
    }

    @Test
    public void testChangingItemQuantityOfNull() {
        StockItem item = null;
        try {
            warehouse.changeItemQuantity(item, 10);
            fail("changeItemQuantity() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemQuantityOfItemNotPresentInDatabase() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 1);
        try {
            warehouse.changeItemQuantity(item, 10);
            fail("changeItemQuantity() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemQuantityToNegativeValue() {
        StockItem item = dao.findStockItem(4);
        try {
            warehouse.changeItemQuantity(item, -10);
            fail("changeItemQuantity() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemQuantityOfExistingItem() {
        StockItem item = dao.findStockItem(4);
        warehouse.changeItemQuantity(item, 47);

        assertNotNull("Item must be present in the database after saving it.", dao.findStockItem(4));
        assertEquals("Price must be changed", 47, dao.findStockItem(4).getQuantity());
    }

    @Test
    public void testChangingItemPriceBeginsAndCommitsTransaction() {
        StockItem item = dao.findStockItem(4);
        warehouse.changeItemPrice(item, 10);
        List<String> methodsCalled = dao.getMethodsCalled();

        assertFalse("saveStockItem() must not be called.", dao.getMethodsCalled().contains("saveStockItem"));
        assertFalse("rollbackTransaction must not be called.", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction() and commitTransaction() must each be called once.", 2, methodsCalled.size());
        assertEquals("beginTransaction() must be called first.", "beginTransaction", methodsCalled.get(0));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing.", "commitTransaction", methodsCalled.get(1));
    }

    @Test
    public void testChangingItemPriceOfNull() {
        StockItem item = null;
        try {
            warehouse.changeItemPrice(item, 10);
            fail("changeItemPrice() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemPriceOfItemNotPresentInDatabase() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 1);
        try {
            warehouse.changeItemPrice(item, 10);
            fail("changeItemPrice() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemPriceToNegativeValue() {
        StockItem item = dao.findStockItem(4);
        try {
            warehouse.changeItemPrice(item, -10);
            fail("changeItemPrice() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testChangingItemPriceOfExistingItem() {
        StockItem item = dao.findStockItem(4);
        warehouse.changeItemPrice(item, 100.5);

        assertNotNull("Item must be present in the database after saving it.", dao.findStockItem(4));
        assertEquals("Price must be changed", 100.5, dao.findStockItem(4).getPrice(), 0.0000001);
    }

    @Test
    public void testAddingExpiryDateBeginsAndCommitsTransaction() {
        warehouse.addExpiryDate(4L, LocalDate.of(2020, 1, 18));
        List<String> methodsCalled = dao.getMethodsCalled();

        assertFalse("saveStockItem() must not be called.", dao.getMethodsCalled().contains("saveStockItem"));
        assertFalse("rollbackTransaction must not be called.", methodsCalled.contains("rollbackTransaction"));
        assertEquals("beginTransaction() and commitTransaction() must each be called once.", 2, methodsCalled.size());
        assertEquals("beginTransaction() must be called first.", "beginTransaction", methodsCalled.get(0));
        assertEquals("Transaction must be closed by calling commitTransaction() as the last thing.", "commitTransaction", methodsCalled.get(1));
    }

    @Test
    public void testAddingExpiryDateToExistingItem() {
        warehouse.addExpiryDate(4L, LocalDate.of(2020, 1, 18));

        assertTrue("Date must be present in the database after saving it.", dao.findStockItem(4).getExpiryDates().contains(LocalDate.of(2020, 1, 18)));
    }

    @Test
    public void testAddingExistingExpiryDateToExistingItem() {
        warehouse.addExpiryDate(4L, LocalDate.of(2020, 1, 18));
        dao.resetMethods();

        try {
            warehouse.addExpiryDate(4L, LocalDate.of(2020, 1, 18));
            fail("addExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertEquals("Existing expiry date must not be added.", 1, dao.findStockItem(4).getExpiryDates().size());
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testAddingExpiryDateToNull() {
        try {
            warehouse.addExpiryDate(-1L, LocalDate.of(2020, 1, 18));
            fail("addExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testAddingExpiryDateToNonExistingItem() {
        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 1);
        try {
            warehouse.addExpiryDate(5L, LocalDate.of(2020, 1, 18));
            fail("addExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("Expiry Date must not be in the expiry date list.", item.getExpiryDates().contains(LocalDate.of(2020, 1, 18)));
        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testAddingExpiryDateOfNull() {
        try {
            warehouse.addExpiryDate(4L, null);
            fail("addExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("Null must not be in the expiry date list.", dao.findStockItem(4).getExpiryDates().contains(null));
        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingExpiryDateFromNull() {
        try {
            warehouse.removeExpiryDate(-1L, LocalDate.of(2020, 1, 18));
            fail("removeExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingExpiryDateOfNull() {
        try {
            warehouse.removeExpiryDate(4L, null);
            fail("removeExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingExistingExpiryDateFromExistingItem() {
        warehouse.addExpiryDate(4L, LocalDate.of(2020, 1, 18));

        warehouse.removeExpiryDate(4L, LocalDate.of(2020, 1, 18));

        assertFalse("Expiry Date must not be in the expiry date list.", dao.findStockItem(4).getExpiryDates().contains(LocalDate.of(2020, 1, 18)));
    }

    @Test
    public void testRemovingExistingExpiryDateFromNonExistingItem() {
        try {
            warehouse.addExpiryDate(5L, LocalDate.of(2020, 1, 18));
            fail("removeExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }

    @Test
    public void testRemovingNonExistingExpiryDate() {
        try {
            warehouse.removeExpiryDate(4L, LocalDate.of(2020, 1, 18));
            fail("removeExpiryDate() must throw an exception");
        }catch (Exception ignored){
        }

        assertFalse("commitTransaction() must not be called.", dao.getMethodsCalled().contains("commitTransaction"));
        assertEquals("rollbackTransaction() must be called.", dao.getMethodsCalled().get(1), "rollbackTransaction");
    }
}
