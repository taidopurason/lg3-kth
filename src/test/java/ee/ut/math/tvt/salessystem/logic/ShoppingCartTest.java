package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShoppingCartTest {
    private class MockDAO extends InMemorySalesSystemDAO {
        private final List<String> methodsCalled = new ArrayList<>();
        private final List<StockItem> stockItemList;

        public MockDAO(List<StockItem> stockItemList) {
            this.stockItemList = stockItemList;
        }

        public MockDAO() {
            List<StockItem> items = new ArrayList<StockItem>();
            items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
            items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
            items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
            items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            this.stockItemList = items;
        }

        public void resetMethods(){
            methodsCalled.clear();
        }

        public void resetStockItems(boolean addDefaultItems){
            stockItemList.clear();
            if (addDefaultItems){
                stockItemList.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
                stockItemList.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
                stockItemList.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
                stockItemList.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            }
        }

        public List<String> getMethodsCalled() {
            return methodsCalled;
        }

        @Override
        public void beginTransaction() {
            methodsCalled.add("beginTransaction");
        }

        @Override
        public void rollbackTransaction() {
            methodsCalled.add("rollbackTransaction");
        }

        @Override
        public void saveStockItem(StockItem item) {
            methodsCalled.add("saveStockItem");
            super.saveStockItem(item);
        }

        @Override
        public void removeStockItem(StockItem item) {
            methodsCalled.add("removeStockItem");
            super.removeStockItem(item);
        }

        @Override
        public void commitTransaction() {
            methodsCalled.add("commitTransaction");
        }
    }

    @Test
    public void testAddingExistingItem() {
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);
        dao.saveStockItem(item);
        SoldItem itemS = new SoldItem(item, 1);
        SoldItem itemS2 = new SoldItem(item ,3);
        cart.addItem(itemS);
        cart.addItem(itemS2);

        dao.resetMethods();
        SoldItem itemFromCart = cart.findMatchingItemsInCart(itemS).get(0);

        assertNotNull("Item must be present in the Shopping cart after adding it.", itemFromCart);
        assertEquals("Quantity of item in Shopping cart is not correct.", 4, (int) itemFromCart.getQuantity());
    }

    @Test
    public void testAddingNewItem(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);
        dao.saveStockItem(item);
        SoldItem itemS = new SoldItem(item, 1);
        cart.addItem(itemS);

        dao.resetMethods();
        SoldItem itemFromCart = cart.findMatchingItemsInCart(itemS).get(0);

        assertNotNull("Item must be present in the shopping cart after saving it.", itemFromCart);
        assertEquals("IDs must match.", itemS.getId(), itemFromCart.getId());
        assertEquals("Names must match.", itemS.getName(), itemFromCart.getName());
        assertEquals("Prices must match.", itemS.getPrice(), itemFromCart.getPrice(), 0.0000001);
        assertEquals("Quantities must match.", itemS.getQuantity(), itemFromCart.getQuantity());
    }
    @Test
    public void testAddingItemWithNegativeQuantity(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 100);
        dao.saveStockItem(item);
        SoldItem itemS = new SoldItem(item, -1);
        try {
            cart.addItem(itemS);
            fail("addItem() must throw an exception when adding negative quantity of product to Cart");
        }catch (Exception ignored){
            //Check that product is not in Cart
            assertEquals("Exception thrown but product with negative quantity in cart",cart.findMatchingItemsInCart(itemS).size(),0);
        }

    }

    @Test
    public void testAddingItemWithQuantityTooLarge(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 10);
        dao.saveStockItem(item);
        SoldItem itemS = new SoldItem(item, 12);
        try {
            cart.addItem(itemS);
            fail("addItem() must throw an exception when adding quantity of product to Cart which is larger than in stock");
        }catch (Exception ignored){
            //Check that product is not in Cart
            assertEquals("Exception thrown but product with too large quantity in cart",cart.findMatchingItemsInCart(itemS).size(),0);
        }
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 0.0, 10);
        dao.saveStockItem(item);
        SoldItem itemS = new SoldItem(item, 6);
        cart.addItem(itemS);
        try {
            cart.addItem(new SoldItem(item, 10));
            fail("addItem() must throw an exception when adding quantity of product to Cart which is larger than in stock");
        }catch (Exception ignored){
            //Check that product is not in Cart
            assertEquals("Exception thrown but product with too large quantity in cart", (long) cart.findMatchingItemsInCart(itemS).get(0).getQuantity(),6);
        }
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);
        StockItem item2 = new StockItem(6L, "Walter", "5.5% Pilsner", 1.5, 10);
        dao.saveStockItem(item2);
        StockItem item3 = new StockItem(7L, "Säästu Kange", "7.5% Dark", 1.75, 8);
        dao.saveStockItem(item3);

        SoldItem itemS = new SoldItem(item, 1);
        SoldItem itemS2 = new SoldItem(item2, 4);
        SoldItem itemS3 = new SoldItem(item3, 5);
        cart.addItem(itemS);
        cart.addItem(itemS2);
        cart.addItem(itemS3);
        cart.submitCurrentPurchase("Credit");

        dao.resetMethods();

        assertEquals("Quantity of "+item.getName()+" not correct", item.getQuantity(), 99, 0.0000001);
        assertEquals("Quantity of "+item2.getName()+" not correct", item2.getQuantity(), 6, 0.0000001);
        assertEquals("Quantity of "+item3.getName()+" not correct", item3.getQuantity(), 3, 0.0000001);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction(){
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);
        StockItem item2 = new StockItem(6L, "Walter", "5.5% Pilsner", 1.5, 10);
        dao.saveStockItem(item2);
        StockItem item3 = new StockItem(7L, "Säästu Kange", "7.5% Dark", 1.75, 8);
        dao.saveStockItem(item3);
        //Reset calls to dao so that adding items to stock wouldn't affect the result
        dao.resetMethods();
        SoldItem itemS = new SoldItem(item, 1);
        SoldItem itemS2 = new SoldItem(item2, 4);
        SoldItem itemS3 = new SoldItem(item3, 5);
        cart.addItem(itemS);
        cart.addItem(itemS2);
        cart.addItem(itemS3);

        cart.submitCurrentPurchase("Credit");

        assertEquals("Too large number of calls to DAO",2, dao.methodsCalled.size(), 0.0000001);
        assertTrue("beginTransaction not in correct order", dao.methodsCalled.get(0).equals("beginTransaction"));
        assertTrue("commitTransaction not in correct order", dao.methodsCalled.get(1).equals("commitTransaction"));
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem(){
        //Our Historyitem is named purchase
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);
        StockItem item2 = new StockItem(6L, "Walter", "5.5% Pilsner", 1.5, 10);
        dao.saveStockItem(item2);
        StockItem item3 = new StockItem(7L, "Säästu Kange", "7.5% Dark", 1.75, 8);
        dao.saveStockItem(item3);
        //Reset calls to dao so that adding items to stock wouldn't affect the result
        dao.resetMethods();
        SoldItem itemS = new SoldItem(item, 1);
        SoldItem itemS2 = new SoldItem(item2, 4);
        SoldItem itemS3 = new SoldItem(item3, 5);
        ArrayList<SoldItem> addedList = new ArrayList<SoldItem>();
        addedList.add(itemS);
        addedList.add(itemS2);
        addedList.add(itemS3);
        cart.addItem(itemS);
        cart.addItem(itemS2);
        cart.addItem(itemS3);

        cart.submitCurrentPurchase("Credit");

        Purchase lastPurchase = dao.getPurchaseList().get(dao.getPurchaseList().size()-1);


        assertNotNull("Purchase must be present in the database after saving it.", lastPurchase);
        for(int i=0;i<lastPurchase.getSoldItems().size();i++) {
            assertEquals("IDs of items in Purchase must match with items added to Cart.", addedList.get(i).getId(), lastPurchase.getSoldItems().get(i).getId());
            assertEquals("Names of items in Purchase must match with items added to Cart.", addedList.get(i).getName(), lastPurchase.getSoldItems().get(i).getName());
            assertEquals("Prices of items in Purchase must match with items added to Cart.", addedList.get(i).getPrice(), lastPurchase.getSoldItems().get(i).getPrice(), 0.0000001);
            assertEquals("Quantities of items in Purchase must match with items added to Cart.", (int) addedList.get(i).getQuantity(), (int) lastPurchase.getSoldItems().get(i).getQuantity());
        }
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime (){
        //Our Historyitem is named purchase
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);
        //Reset calls to dao so that adding items to stock wouldn't affect the result
        dao.resetMethods();
        SoldItem itemS = new SoldItem(item, 1);
        cart.addItem(itemS);
        long currentTime = System.currentTimeMillis();
        cart.submitCurrentPurchase("Credit");

        Purchase lastPurchase = dao.getPurchaseList().get(dao.getPurchaseList().size()-1);

        assertNotNull("Purchase must be present in the database after saving it.", lastPurchase);
        assertEquals("Timestamp not correct", lastPurchase.getDate().getTime(), currentTime, 1);
    }

    @Test
    public void testCancellingOrder(){
        //Our Historyitem is named purchase
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);
        StockItem item2 = new StockItem(6L, "Walter", "5.5% Pilsner", 1.5, 10);
        dao.saveStockItem(item2);

        //Reset calls to dao so that adding items to stock wouldn't affect the result
        dao.resetMethods();
        SoldItem itemS = new SoldItem(item, 1);
        SoldItem itemS2 = new SoldItem(item2, 4);

        cart.addItem(itemS);
        cart.cancelCurrentPurchase();

        cart.addItem(itemS2);
        cart.submitCurrentPurchase("Credit");

        Purchase lastPurchase = dao.getPurchaseList().get(dao.getPurchaseList().size()-1);


        assertNotNull("Purchase must be present in the database after saving it.", lastPurchase);
        assertEquals("Too many items in cart", lastPurchase.getSoldItems().size(), 1);
        for(int i=0;i<lastPurchase.getSoldItems().size();i++) {
            assertEquals("IDs must match with items added to Cart after cancellation.", itemS2.getId(), lastPurchase.getSoldItems().get(i).getId());
            assertEquals("Names must match with items added to Cart after cancellation.", itemS2.getName(), lastPurchase.getSoldItems().get(i).getName());
            assertEquals("Prices must match with items added to Cart after cancellation.", itemS2.getPrice(), lastPurchase.getSoldItems().get(i).getPrice(), 0.0000001);
            assertEquals("Quantities must match with items added to Cart after cancellation.", (int) itemS2.getQuantity(), (int) lastPurchase.getSoldItems().get(i).getQuantity());
        }
    }

    @Test
    public void testCancellingOrderQuantitiesUnchanged(){
        //Our Historyitem is named purchase
        ShoppingCartTest.MockDAO dao = new ShoppingCartTest.MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);

        StockItem item = new StockItem(5L, "Põhjala Uus Maailm", "4.7% San Diego Session IPA", 3.0, 100);
        dao.saveStockItem(item);

        //Reset calls to dao so that adding items to stock wouldn't affect the result
        dao.resetMethods();
        SoldItem itemS = new SoldItem(item, 1);

        cart.addItem(itemS);
        cart.cancelCurrentPurchase();

        //Get the same item from database after cancellation
        StockItem item2 = dao.findStockItem(5L);

        assertEquals("Wrong number of item in Stock", item2.getQuantity(), 100);
    }


}
