package ee.ut.math.tvt.salessystem.logic;

import static org.junit.Assert.*;
import org.junit.Test;

public class PaymentTest {

    @Test
    public void testNotEnoughCashGiven(){
        Payment payment = new Payment(100);
        payment.setCashPayment(true);
        float returnMoney = payment.calculateReturn(20);
        assertEquals("Wrong value return when not enough cash was given", -1, returnMoney, 0.002);
        assertFalse("Payment was success even though not enough cash was given", payment.isPaymentSuccess());
        assertTrue("Payment type was not correct", payment.cashOrCredit().equals("Cash"));
    }

    @Test
    public void testEnoughCashGiven(){
        Payment payment = new Payment(100);
        payment.setCashPayment(true);
        float returnMoney = payment.calculateReturn(120);
        assertEquals("Wrong value return when not enough cash was given", 20, returnMoney, 0.002);
        assertTrue("Payment was not success even though enough cash was given", payment.isPaymentSuccess());
        assertTrue("Payment type was not correct", payment.cashOrCredit().equals("Cash"));
    }

    @Test
    public void testCreditPaymentPINCorrect(){
        Payment payment = new Payment(100);
        payment.setCashPayment(false);
        payment.checkPIN("1923");
        assertTrue("Payment was not success even though credit payment was made", payment.isPaymentSuccess());
        assertTrue("Payment type was not correct", payment.cashOrCredit().equals("Credit"));
    }

    @Test
    public void testCreditPaymentPINWrong(){
        Payment payment = new Payment(100);
        payment.setCashPayment(false);
        payment.checkPIN("23");
        assertFalse("Payment was success even though credit payment was not made", payment.isPaymentSuccess());
        assertTrue("Payment type was not correct", payment.cashOrCredit().equals("Credit"));
    }


}
