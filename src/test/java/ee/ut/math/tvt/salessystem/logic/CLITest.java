package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

public class CLITest {
    private MockDAO dao;
    private CLILogic CLILogic;

    private class MockDAO extends InMemorySalesSystemDAO {
        private final List<String> methodsCalled = new ArrayList<>();
        private final List<StockItem> stockItemList;
        private final List<Employee> employeeList;

        public MockDAO(List<StockItem> stockItemList, List<Employee> employeeList) {
            this.stockItemList = stockItemList;
            this.employeeList = employeeList;
        }

        public MockDAO() {
            List<StockItem> items = new ArrayList<StockItem>();
            items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
            items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
            items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
            items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            this.stockItemList = items;

            Employee employee1 = new Employee("Heiko Tamm", "Manager", "Tartu Village", "+37253535681", "tomtam@hotmail.com");
            Employee employee2 = new Employee("Karin Manna", "Cashier", "Elva City", "+37269696969", "mannakarin@gmail.com");
            Employee employee3 = new Employee("Maido Puran", "Recruiter", "Tartu Village", "+37255555555", "maodi@gmail.com");

            List<Employee> employeeList = new ArrayList<Employee>();
            employeeList.add(employee1);
            employeeList.add(employee2);
            employeeList.add(employee3);
            this.employeeList = employeeList;
        }

        public void resetMethods() {
            methodsCalled.clear();
        }

        public void resetStockItems(boolean addDefaultItems) {
            stockItemList.clear();
            if (addDefaultItems) {
                stockItemList.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
                stockItemList.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
                stockItemList.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
                stockItemList.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
            }
        }

        public List<String> getMethodsCalled() {
            return methodsCalled;
        }

        @Override
        public void beginTransaction() {
            methodsCalled.add("beginTransaction");
        }

        @Override
        public void rollbackTransaction() {
            methodsCalled.add("rollbackTransaction");
        }

        @Override
        public void commitTransaction() {
            methodsCalled.add("commitTransaction");
        }
    }

    @Before
    public void initializeDAOandCLILogic() {
        dao = new MockDAO();
        ShoppingCart cart = new ShoppingCart(dao);
        Warehouse warehouse = new Warehouse(dao);
        EmployeeLogic employeeLog = new EmployeeLogic(dao);
        History history = new History(dao);
        CLILogic = new CLILogic(warehouse, cart, employeeLog, history);
    }

    @Test
    public void testCheckItemInList () {
        StockItem stockItem = dao.findStockItems().get(0);
        boolean returnedValue = CLILogic.checkIfNotInList(stockItem.getId(), stockItem.getName());
        assertEquals("The method must return false that item with ID 1 and the same name is already in stock!", false, returnedValue);
    }

    //bad test because changing input means having to change this test aswell?
    @Test
    public void testStockItemFromInput () {
        StockItem returnedItem = CLILogic.getStockItemFromInput("1", "Lays chips");
        StockItem expectedItem = dao.findStockItems().get(0); //lays chips is in mockdao always 0

        assertEquals("The method must return Lays chips StockItem from parsing pre-defined input!", expectedItem, returnedItem);
    }

    @Test
    public void testGetEmployeeList () {
        List<Employee> returnedList = CLILogic.getEmployeeList();
        List<Employee> expectedList = dao.getEmployeeList();

        assertEquals("The employees list from CLILogic has to equal the list from DAO.", expectedList, returnedList);
    }




}