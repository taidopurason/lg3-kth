package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;

@Entity
@Table
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    @Column
    private String name;
    @Column
    private String position;
    @Column
    private String address;
    @Column
    private String phoneNumber;
    @Column
    private String email;
//    private String[] shifts;

    public Employee (){};

    public Employee (String name, String position, String address, String phoneNumber, String email) {
        this.name = name;
        this.position = position;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getName() {return name;}

    public String getPosition() {return position;}

    public String getAddress() {return address;}

    public String getPhoneNumber() {return phoneNumber;}

    public String getEmail() {return email;}

//    public String[] getShifts() {return shifts;}

    public void setName(String name) {this.name = name;}

    public void setPosition(String position) {this.position = position;}

    public void setAddress(String address) {this.address = address;}

    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    public void setEmail(String email) {this.email = email;}

    public long getID(){return ID;}

//    public void setShifts(String[] shifts) {this.shifts = shifts;}

    public String toString() {return name + ", " + position;};

    public String showData(){
        StringBuffer toPrint = new StringBuffer();
        toPrint.append("Address: "+address+"\n");
        toPrint.append("Phone Number: "+phoneNumber+"\n");
        toPrint.append("E-mail: "+email+"\n");
        return toPrint.toString();
    }

}
