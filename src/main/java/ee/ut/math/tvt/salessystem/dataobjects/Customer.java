package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;

@Entity
@Table
public class Customer {

    @Id
    private long clientCardID;
    @Column
    private String name;
    @Column
    private String contactInfo;

    public Customer(String name, long clientCardID, String contactInfo) {
        this.name = name;
        this.clientCardID = clientCardID;
        this.contactInfo = contactInfo;
    }
    //Default constructor
    public Customer(){};

    public String getName() {
        return this.name;
    }

    public long getClientCardID() {
        return this.clientCardID;
    }

    public String getContactInfo() {
        return this.contactInfo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClientCardID(long clientCardID) {
        this.clientCardID = clientCardID;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String toString() {
        return "Customer:[Name:{" + this.name + "} ClientCardID:{" + this.clientCardID + "}]";
    }
}
