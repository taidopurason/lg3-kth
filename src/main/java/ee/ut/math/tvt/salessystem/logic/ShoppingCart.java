package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private Purchase lastPurchase = null;
    private Customer loyalCustomer;
    private boolean isLoyalCostumer;
    private boolean discountsAdded;
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        log.debug("initializing ShoppingCart ");
        this.dao = dao;
        isLoyalCostumer = false;
        discountsAdded = false;
    }

    //Calculate price of the shopping cart
    public float calculatePrice(){
        float cartPrice = 0;
        for (SoldItem item : items) {
            //Calculate price
            cartPrice+=item.getPrice()*item.getQuantity();
        }
        return (float) (Math.round(cartPrice*100)/100.0);
    }

    public Customer checkCustomer(Customer data){
        dao.beginTransaction();
        loyalCustomer = dao.findCustomer(data);
        if(loyalCustomer!=null){
            isLoyalCostumer = true;
            addDiscounts();
        }
        dao.commitTransaction();
        return loyalCustomer;
    }

    //Add discounts to products where it has not been added, discount is 5% for each product's original price
    private void addDiscounts(){
        if(isLoyalCostumer && !discountsAdded){
            for(SoldItem item : items){
                if(item.getOriginalPrice()*0.95<item.getPrice()){
                    item.setPrice(Math.round(item.getOriginalPrice()*0.95*100)/100.0);
                }
            }
            discountsAdded=true;
        }
    }

    public List<SoldItem> findMatchingItemsInCart(SoldItem item) {
        return items.stream().filter(cartItem -> cartItem.getStockItem() == item.getStockItem()).collect(Collectors.toList());
    }

    private int itemQuantities(List<SoldItem> items) {
        if (items == null || items.size() == 0) return 0;
        return items.stream().mapToInt(SoldItem::getQuantity).sum();
    }

    private SoldItem findFirstItemWithMatchingPrice(List<SoldItem> items, SoldItem item){
        return items.stream().filter(cartItem -> cartItem.getPrice() == item.getPrice()).findFirst().orElse(null);
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        // In case such stockItem already exists increase the quantity of the existing stock (if the price matches)
        // verify that warehouse items' quantity remains at least zero or throw an exception
        if (item == null || item.getStockItem() == null || !dao.findStockItems().contains(item.getStockItem()))
            throw new SalesSystemException("Item not in stock.");

        if (item.getQuantity() < 0) throw new SalesSystemException("Quantity must be non-negative");
        if (item.getPrice() < 0) throw new SalesSystemException("Price must be non-negative");
        //If person is loyal costumer and loyal costumer discount is better than the cashier entered use loyal costumer discount
        if (isLoyalCostumer && item.getPrice() > item.getOriginalPrice()*0.95) item.setPrice(item.getOriginalPrice()*0.95);

        List<SoldItem> matchingCartItems = findMatchingItemsInCart(item);
        if (item.getStockItem().getQuantity() < item.getQuantity() + itemQuantities(matchingCartItems))
            throw new SalesSystemException("Not enough of item in stock.");

        // make sure the price matches
        SoldItem matchingCartItemWithMatchingPrice = findFirstItemWithMatchingPrice(matchingCartItems, item);
        if (matchingCartItemWithMatchingPrice != null)
            matchingCartItemWithMatchingPrice.setQuantity(matchingCartItemWithMatchingPrice.getQuantity() + item.getQuantity());
        else items.add(item);
        log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    public void removeItem(SoldItem item) {
        SoldItem matchingItem = findFirstItemWithMatchingPrice(findMatchingItemsInCart(item), item);
        if (item == null || matchingItem == null)
            throw new SalesSystemException("Item not in cart.");


        if (item.getQuantity() < 0) throw new SalesSystemException("Quantity must be non-negative");
        if (item.getPrice() < 0) throw new SalesSystemException("Price must be non-negative");

        if (matchingItem.getQuantity() < item.getQuantity()) throw new SalesSystemException("Trying to remove more than there is in shopping cart");

        if(matchingItem.getQuantity().equals(item.getQuantity())) items.remove(matchingItem);
        else matchingItem.setQuantity(matchingItem.getQuantity() - item.getQuantity());
        log.debug("Removed " + item.getName() + " quantity of " + item.getQuantity());
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public Customer getLoyalCustomer(){
        return loyalCustomer;
    }

    public Purchase getLastPurchase(){return lastPurchase;}

    public void cancelCurrentPurchase() {
        log.debug("Purchase cancelled");
        clearShoppingCart();
    }

    public void editQuantity(SoldItem item, int quantity){
        if (item == null || !items.contains(item)) throw new SalesSystemException("Item not in cart.");
        if (quantity < 0) throw new SalesSystemException("Quantity can't be negative");
        if (quantity == 0) items.remove(item);
        if (item.getStockItem().getQuantity() < quantity + itemQuantities(findMatchingItemsInCart(item)) - item.getQuantity())
            throw new SalesSystemException("Not enough of item in stock.");
        item.setQuantity(quantity);
    }

    public void submitCurrentPurchase(String paymentMethod) {

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            lastPurchase = new Purchase(new ArrayList<SoldItem>(items), paymentMethod);
            dao.savePurchase(lastPurchase);
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
                item.getStockItem().setQuantity(item.getStockItem().getQuantity() - item.getQuantity()); // will set the quantity in stock minus the quantity sold
                //Diminish the number of products also in database
                dao.updateStockItem(item.getStockItem());
            }
            //ID needs to be changed.
            dao.commitTransaction();
            clearShoppingCart();
        } catch (Exception e) {
            log.error("Rolling back transaction, exception occurred in ShoppingCart: " + e);
            dao.rollbackTransaction();
            throw e;
        }
        log.debug("Purchase submitted");
    }

    private void clearShoppingCart(){
        items.clear();
        isLoyalCostumer = false;
        discountsAdded = false;
    }
}
