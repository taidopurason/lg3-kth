package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Employee> employeeList;
    private final List<Purchase> purchaseList;
    private final List<Customer> customerList;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();

        Employee employee1 = new Employee("Heiko Tamm", "Manager", "Tartu Village", "+37253535681", "tomtam@hotmail.com");
        Employee employee2 = new Employee("Karin Manna", "Cashier", "Elva City", "+37269696969", "mannakarin@gmail.com");
        Employee employee3 = new Employee("Maido Puran", "Recruiter", "Tartu Village", "+37255555555", "maodi@gmail.com");

        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        this.employeeList = employeeList;


        Customer customer1 = new Customer("Mati Maasikas",391L,"");
        Customer customer2 = new Customer("Meri Kaasik",392L,"");
        Customer customer3 = new Customer("Enn Vetel",393L,"");
        Customer customer4 = new Customer("Fernando Maloon",394L,"");
        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);
        this.customerList = customerList;

        List<SoldItem> soldItems = new ArrayList<SoldItem>();
        soldItems.add(new SoldItem(items.get(0), 2));
        soldItems.add(new SoldItem(items.get(3), 1));
        Purchase purchase1 = new Purchase(1L, soldItems, "Cash");
        List<SoldItem> soldItems2 = new ArrayList<SoldItem>();
        soldItems2.add(new SoldItem(items.get(1), 5));
        soldItems2.add(new SoldItem(items.get(0), 1));
        soldItems2.add(new SoldItem(items.get(3), 20));
        Purchase purchase2 = new Purchase(2L, soldItems2, "Credit");
        List<SoldItem> soldItems3 = new ArrayList<SoldItem>();
        soldItems3.add(new SoldItem(items.get(0), 1));
        Purchase purchase3 = new Purchase(3L, soldItems3, "Cash");
        List<Purchase> purchaseList = new ArrayList<>();
        purchaseList.add(purchase1);
        purchaseList.add(purchase2);
        purchaseList.add(purchase3);
        this.purchaseList = purchaseList;

    }

    @Override
    public List<Employee> getEmployeeList() {return employeeList;}


    @Override
    public Employee getEmployee(long ID){
        for (Employee e : employeeList) {
            if (e.getID() == ID) {
                return e;
            }
        }
        return null;
    }

    @Override
    public void removeEmployee(Employee e) {
        employeeList.remove(e);
    }

    @Override
    public List<Purchase> getPurchaseList() {return purchaseList;}

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public void removeStockItem(StockItem stockItem){
        for (StockItem item : stockItemList){
            if(item.getId().equals(stockItem.getId())){
                stockItemList.remove(item);
                break;
            }
        }
    }

    @Override
    public Customer findCustomer(Customer clientData){
            for (Customer client : customerList){
            //Try first with customer name
            try{
                if(client.getName().equals(clientData.getName())){
                    return client;
                }
            }
            catch(Exception e){ }
            //Then with ID
            try{
                if(client.getClientCardID()==clientData.getClientCardID()){
                    return client;
                }
            }
            catch(Exception e){ }
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void savePurchase(Purchase purchase) {purchaseList.add(purchase);}

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }

    public void addExpiryDate(ExpiryDate date){};

    public void removeExpiryDate(ExpiryDate date){};

    public void updateStockItem(StockItem item){};

    @Override
    public List<Purchase> getLast10Purchases(){
        if(purchaseList.size()<=10){
            return purchaseList;
        }
        return purchaseList.subList(purchaseList.size()-10,purchaseList.size());
    }

    public List<Purchase> getPurchasesBetweenDates(Date startDate, Date endDate){
        List<Purchase> returnList = new ArrayList<Purchase>();
        for(int i=0;i<purchaseList.size();i++){
            if(purchaseList.get(i).getDate().getTime()<=endDate.getTime() && purchaseList.get(i).getDate().getTime()>=startDate.getTime()){
                returnList.add(purchaseList.get(i));
            }
        }
        return returnList;}
}
