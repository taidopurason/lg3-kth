package ee.ut.math.tvt.salessystem.logic;

public class Payment {

    private float amountToPay;
    private boolean cashPayment; //True if cash payment, false if credit payment
    private boolean paymentSuccess = false; //True if payment successful

    public Payment(float amountToPay){
        this.amountToPay = amountToPay;
    }

    //Methods for carrying out payment

    //Calculate return money
    public float calculateReturn(float cashGiven){
        if(cashGiven<amountToPay){
            return -1;
        }
        paymentSuccess = true;
        return (float) (Math.round((cashGiven - amountToPay)*100.0)/100.0);
    }
    /*
    public boolean checkPIN(int pin){
        //Check if the pin is 4 digit number
        if(Integer.toString(pin).length()==4){
            paymentSuccess = true;
            return true;
        }
        return false;
    }
     */
    public boolean checkPIN(String pin){
        if(pin.length()==4){
            paymentSuccess = true;
            return true;
        }
        return false;
    }
    public String cashOrCredit(){
        if(cashPayment){
            return "Cash";
        }
        return "Credit";
    }

    public void setCashPayment(boolean type){
        this.cashPayment=type;
    }

    public boolean isPaymentSuccess(){
        return paymentSuccess;
    }

    public boolean isCashPayment(){
        return cashPayment;
    }

    public float getAmountToPay(){
        return amountToPay;
    }

}
