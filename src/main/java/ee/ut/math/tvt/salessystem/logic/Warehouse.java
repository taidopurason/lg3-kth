package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.ExpiryDate;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Warehouse {
    private final SalesSystemDAO dao;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addItem(StockItem item){
        dao.beginTransaction();
        try {
            if(item.getQuantity() < 0) throw new SalesSystemException("Quantity must not be negative.");
            if(item.getPrice() < 0) throw new SalesSystemException("Price must not be negative.");
            StockItem itemWithSameName = dao.findStockItem(item.getName());
            StockItem itemWithSameId = dao.findStockItem(item.getId());
            if (itemWithSameId == itemWithSameName && itemWithSameId != null) { // change .
                itemWithSameName.setQuantity(itemWithSameName.getQuantity() + item.getQuantity());
            } else { // add item if it doesn't exist.
                if(itemWithSameId != null) throw new SalesSystemException("Item with id " + item.getId().toString() + " is already in stock."); // ItemExistsException
                if(itemWithSameName != null) throw new SalesSystemException(item.getName() + "is already in stock."); // ItemExistsException
                dao.saveStockItem(item);
            }
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }

    }

    public void removeItem(StockItem item) {
        dao.beginTransaction();
        try {
            if(item == null || !dao.findStockItems().contains(item)) throw new SalesSystemException("Item is not in stock."); // ItemNotInStockException
            dao.removeStockItem(item);
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void changeItemQuantity(StockItem item, int newQuantity) {
        dao.beginTransaction();
        try {
            if(newQuantity < 0) throw new SalesSystemException("Quantity must not be negative."); // IllegalQuantityException
            if(item == null || !dao.findStockItems().contains(item)) throw new SalesSystemException("Item is not in stock."); // ItemNotInStockException
            item.setQuantity(newQuantity);
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void changeItemPrice(StockItem item, double newPrice) {
        dao.beginTransaction();
        try {
            if(newPrice < 0) throw new SalesSystemException("Price must not be negative."); // IllegalPriceException
            if(item == null || !dao.findStockItems().contains(item)) throw new SalesSystemException("Item is not in stock."); // ItemNotInStockException
            item.setPrice(newPrice);
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }


        public void addExpiryDate(long id, LocalDate date){
        dao.beginTransaction();
        StockItem item = dao.findStockItem(id);
        try {
            if(item == null || !dao.findStockItems().contains(item)) throw new SalesSystemException("Item is not in stock."); // ItemNotInStockException
            if(date == null) throw new SalesSystemException("Date not valid."); // IllegalPriceException
            if(item.getExpiryDates().contains(date)) throw new SalesSystemException("Expiry date is already in list.");
            ExpiryDate Expdate = new ExpiryDate();
            Expdate.setExpiryDate(date);
            dao.addExpiryDate(Expdate);
            dao.commitTransaction();
            item.addExpiryDate(Expdate);
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void removeExpiryDate(long id, LocalDate date) {
        dao.beginTransaction();
        StockItem item = dao.findStockItem(id);
        try {
            if(item == null || !dao.findStockItems().contains(item)) throw new SalesSystemException("Item is not in stock."); // ItemNotInStockException
            if(date == null) throw new SalesSystemException("Date not valid."); // IllegalPriceException
            if(!item.getExpiryDates().contains(date)) throw new SalesSystemException("Expiry date is not in list.");
            ExpiryDate expDate = item.getExpiryDate(date);
            //Remove from database and synchronise current item
            if(expDate!=null) {
                dao.removeExpiryDate(expDate);
                item.removeExpiryDate(expDate);
            }
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public StockItem fetchStockItem(long id){
        dao.beginTransaction();
        StockItem item = dao.findStockItem(id);
        dao.commitTransaction();
        return item;
    }

    public StockItem fetchStockItem(String name){
        dao.beginTransaction();
        StockItem item = dao.findStockItem(name);
        dao.commitTransaction();
        return item;
    }

    public List<StockItem> fetchStockItems(String filter){
        dao.beginTransaction();
        List<StockItem> result = dao.findStockItems().stream().filter(item -> item.getId().toString().equals(filter) || item.getName().toLowerCase().contains(filter.toLowerCase()) || item.getDescription().toLowerCase().contains(filter.toLowerCase())).collect(Collectors.toList());
        dao.commitTransaction();
        return result;
    }

    public List<StockItem> fetchStockItems(){
        dao.beginTransaction();
        List<StockItem> result = dao.findStockItems();
        dao.commitTransaction();
        return result;
    }

}
