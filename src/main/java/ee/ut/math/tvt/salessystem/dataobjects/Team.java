package ee.ut.math.tvt.salessystem.dataobjects;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class Team {

    private String teamName;
    private String contactPerson;
    private String eMail;
    private String members;
    private String imagePath;
    private String motto;

    public Team(String dataPath) throws IOException {
        //Get data from property file "../application.properties"
            InputStream in = new FileInputStream(dataPath);
            Properties props = new Properties();
            props.load(in);
            Set<String> properties = props.stringPropertyNames();
            //Iterator for scanning over properties
            Iterator<String> it = properties.iterator();
            while(it.hasNext()){

                //Get property
                String propertyName = it.next();

                //Assign right value to right variable
                if(propertyName.equals("teamname")){
                    this.teamName = props.getProperty(propertyName);
                }
                else if(propertyName.equals("contactperson")){
                    this.contactPerson = props.getProperty(propertyName);
                }
                else if(propertyName.equals("email")){
                    this.eMail = props.getProperty(propertyName);
                }
                else if(propertyName.equals("members")){
                    this.members = props.getProperty(propertyName);

                }
                else if(propertyName.equals("teamMotto")){
                    this.motto = props.getProperty(propertyName);
                }
                else if(propertyName.equals("teamlogoPath")){
                    this.imagePath = props.getProperty(propertyName);
                }
            }
        }

    //toString() method for giving Team data as String
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("=========================== \n");
        buffer.append("\t\t\t"+teamName+"\n");
        buffer.append("=========================== \n");
        buffer.append("Contact person:\t\t"+contactPerson+"\n");
        buffer.append("E-mail:\t\t\t"+eMail+"\n");
        buffer.append("Team members:\t\t"+members);
        return buffer.toString();
    }

    //Getters
    public String getTeamName() {
        return teamName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public String geteMail() {
        return eMail;
    }

    public String getMembers() {
        return members;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getMotto() {
        return motto;
    }

}
