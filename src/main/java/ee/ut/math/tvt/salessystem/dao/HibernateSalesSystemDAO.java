package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
/*
        /////Default values to be added to Database for testing

        em.getTransaction().begin();
        //StockItems
        em.persist(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        em.persist(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        em.persist(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        em.persist(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        ////Customers
        em.persist(new Customer("Mati Maasikas",391L,""));
        em.persist(new Customer("Meri Kaasik",392L,""));
        em.persist(new Customer("Enn Vetel",393L,""));
        em.persist(new Customer("Fernando Maloon",394L,""));
        //Employees
        em.persist(new Employee("Heiko Tamm", "Manager", "Tartu Village", "+37253535681", "tomtam@hotmail.com"));
        em.persist(new Employee("Karin Manna", "Cashier", "Elva City", "+37269696969", "mannakarin@gmail.com"));
        em.persist(new Employee("Maido Puran", "Recruiter", "Tartu Village", "+37255555555", "maodi@gmail.com"));

        em.getTransaction().commit();
*/
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public List<Employee> getEmployeeList() {
        return em.createQuery("from Employee", Employee.class).getResultList();
    }

    @Override
    public void removeEmployee(Employee e) {
        em.remove(e);
    }

    @Override
    public Employee getEmployee(long ID){return em.createQuery("SELECT e FROM Employee e WHERE e.ID=:id", Employee.class).setParameter("id",
            ID).getSingleResult();}

    @Override
    public List<Purchase> getPurchaseList() {
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return findStockItems().stream().filter(item -> item.getId() == id).findFirst().orElse(null);
    }

    @Override
    public StockItem findStockItem(String name) {
        return findStockItems().stream().filter(item -> item.getName().equals(name)).findFirst().orElse(null);
    }

    @Override
    public void removeStockItem(StockItem stockItem) {
        em.remove(stockItem);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
    }

    @Override
    public void updateStockItem(StockItem item){
        if(item.getQuantity()>-1){
            em.merge(item);
        }
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void addExpiryDate(ExpiryDate date){em.persist(date);}

    public void removeExpiryDate(ExpiryDate date){em.remove(date);};

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void savePurchase(Purchase purchase) {
        em.persist(purchase);
    }

    @Override
    public List<Purchase> getPurchasesBetweenDates(Date startDate, Date endDate){
        return em.createQuery("SELECT p FROM Purchase p WHERE p.purchaseTime BETWEEN :startDate AND :endDate", Purchase.class).setParameter("startDate",
                startDate).setParameter("endDate", endDate).getResultList();}

    @Override
    public List<Purchase> getLast10Purchases(){
        return em.createQuery("SELECT p FROM Purchase p ORDER BY p.purchaseTime DESC", Purchase.class).setMaxResults(10).getResultList();
    }

    @Override
    public Customer findCustomer(Customer clientData) {
        return em.createQuery("SELECT c FROM Customer c WHERE c.clientCardID = :id OR c.name = :name", Customer.class).setParameter("id",
                clientData.getClientCardID()).setParameter("name", clientData.getName()).getSingleResult();
    }
}