package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class ExpiryDate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long expiryDateId;
    @Column
    LocalDate expiryDate;
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "STOCKITEM_ID")
    private StockItem stockitem;

    public ExpiryDate(){}

    public LocalDate getExpiryDate(){
        return expiryDate;
    }

    public void setExpiryDate(LocalDate date){
        this.expiryDate= date;
    }

    public void setStockitem(StockItem stock){
        this.stockitem=stock;
    }

}
