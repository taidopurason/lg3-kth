package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "purchase", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<SoldItem> items;
    @Column
    private Date purchaseTime = new Date(System.currentTimeMillis());
    @Column
    private String paymentMethod;
    @Column(name="totalPrice")
    private double sum;

    public Purchase(List<SoldItem> items, String paymentMethod) {
        this.sum = 0;
        this.items = items;
        this.paymentMethod = paymentMethod;
        //counts the sold items and the prices they were sold at, multiplies it with quantity and adds it to the sum.
        for (SoldItem item : this.items) {
            item.setPurchase(this);
            sum += item.getPrice() * item.getQuantity();
        }
    }

    public Purchase(Long id, List<SoldItem> items, String paymentMethod) {
        this.id = id;
        this.sum = 0;
        this.items = items;
        this.paymentMethod = paymentMethod;
        //counts the sold items and the prices they were sold at, multiplies it with quantity and adds it to the sum.
        for (SoldItem item : this.items) {
            item.setPurchase(this);
            sum += item.getPrice() * item.getQuantity();
        }
    }

    public Purchase(){
    }

    public static Comparator<Purchase> getGetpurchaseComparator() {
        Comparator<Purchase> purchaseComparator = new Comparator<Purchase>() {
            @Override
            public int compare(Purchase e1, Purchase e2) {
                if (e1.getDate().getTime() > e2.getDate().getTime()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        };
        return purchaseComparator;
    }


    public Long getId() {
        return this.id;
    }

    public List<SoldItem> getSoldItems() {
        return this.items;
    }

    public Date getDate() {
        return this.purchaseTime;
    }

    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setItems(List<SoldItem> items) {
        this.items = items;
    }

    public void setDate(Date purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public double getSum() {
        return this.sum;
    }

    public void recalculateSum() {
        this.sum = 0;
        for (SoldItem a : this.items) {
            this.sum = this.sum + a.getPrice() * a.getQuantity();
        }
    }

    public String printReceipt() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(String.format("%20s %5s %20s %5s %10s %5s %15s %5s %10s", "Product name", "|",
                "Item price(Eur)", "|", "Quantity", "|", "Item Discount", "|", "Total"));
        buffer.append("\n");
        buffer.append("-------------------------------------------------------------------------------------------------------------");
        buffer.append("\n");
        for (SoldItem si : items) {
            buffer.append(String.format("%20s %5s %20s %5s %10s %5s %15s %5s %10s", si.getName(), "|",
                    si.getOriginalPrice(), "|", si.getQuantity(), "|", si.calculateDiscount(), "|", si.getTotalPrice()));
            buffer.append("\n");
        }
        buffer.append("-------------------------------------------------------------------------------------------------------------");
        buffer.append("\n");
        buffer.append("TOTAL PRICE OF CART: " + Math.round(sum * 100) / 100.0 + " Euros");
        buffer.append("\n");
        DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        dateFormat.setLenient(false);
        buffer.append("DATE OF PURCHASE: " + dateFormat.format(purchaseTime));
        buffer.append("\n");
        buffer.append("PAYMENT METHOD: " + paymentMethod);
        return buffer.toString();
    }

    public String toString() {
        return "Id: " + this.id + " Method: " + this.paymentMethod + " Items: " + this.items;
    }

    public String printData(){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        dateFormat.setLenient(false);
        return String.format("%5s %2s %7s %2s %15s ", this.id, "|",
                this.paymentMethod, "|", dateFormat.format(purchaseTime));
//        return "Id: " +  + " Method: " +  + "Date:" + ;
    }
}
