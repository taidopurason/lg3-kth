package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Employee;

import java.util.List;

public class EmployeeLogic {

    private final SalesSystemDAO dao;

    public EmployeeLogic(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<Employee> getEmployeeList() {
        dao.beginTransaction();
        List<Employee> returnList = dao.getEmployeeList();
        dao.commitTransaction();
        return returnList;
    }

    public void editEmployeeData (long id, String name, String position, String address, String email, String phoneNumber) {
        dao.beginTransaction();
        Employee selected = dao.getEmployee(id);
        selected.setName(name);
        selected.setPosition(position);
        selected.setAddress(address);
        selected.setEmail(email);
        selected.setPhoneNumber(phoneNumber);
        dao.commitTransaction();
    }

    public void removeEmployee (Employee e) {
        dao.beginTransaction();
        dao.removeEmployee(e);
        dao.commitTransaction();
    }

}
