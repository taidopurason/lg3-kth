package ee.ut.math.tvt.salessystem.dataobjects;


import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
@Table
public class SoldItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //@ManyToOne
    //@JoinColumn(nullable=true)
    @Transient
    private StockItem stockItem;
    @Column
    private Long barcode;
    @Column
    private String name;
    @Column
    private Integer quantity;
    @Column
    private double originalPrice;
    @Column
    private double price;
    @ManyToOne
    @JoinColumn
    private Purchase purchase;


    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, int quantity) {
        this(stockItem, quantity, stockItem.getPrice());
    }

    public SoldItem(StockItem stockItem, int quantity, double price) {
        this(stockItem, quantity, price, null);
    }

    public SoldItem(StockItem stockItem, int quantity, double price, Purchase purchase) {
        this.stockItem = stockItem;
        this.barcode = stockItem.getId();
        this.name = getStockItem().getName();
        this.originalPrice = stockItem.getPrice(); //price of the stockItem at the moment of sale.
        this.quantity = quantity;
        this.price = price;
        this.purchase = purchase;

    }

    public Long getId() {
        return stockItem.getId();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double calculateDiscount() {
        return Math.round((price - originalPrice) * 100.0) / 100.0;
    }

    public double calculateTotalDiscountProduct(){
        return quantity*Math.round((price - originalPrice) * 100.0) / 100.0;
    }

    public double getTotalPrice() {
        return Math.round(quantity * price * 100) / 100.0;
    }


    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, name='%s', quantity=%d}", getId(), name, getQuantity());
    }
}
