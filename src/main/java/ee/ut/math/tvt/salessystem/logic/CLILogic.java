package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class CLILogic {

    /**
     * NOTE!
     * This class was created as /possibly/ a temporary solution. These items are maybe meant to be moved around (in to ShoppingCart).
     * Some of the DAO interaction /possibly/ doesn't fit in to ShoppingCart, that's why it's here.
     */
    private static final Logger log = LogManager.getLogger(CLILogic.class);
//    private final SalesSystemDAO dao;
    private Warehouse warehouse;
    private EmployeeLogic employeeLog;
    private ShoppingCart cart;
    private Team meeskond;
    private History history;

    public CLILogic(Warehouse warehouse, ShoppingCart cart, EmployeeLogic employeeLog, History history) {
        this.warehouse = warehouse;
        this.cart=cart;
        this.employeeLog = employeeLog;
        this.history = history;
        try {
            meeskond = new Team("../src/main/resources/application.properties");
        } catch (IOException e) {
            log.error("application.properties file not found");
        }
    }

    public void removeItemFromCart() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.println("Enter bar code: ");
            Long barcode = Long.parseLong(in.readLine().trim());
            StockItem item = warehouse.fetchStockItem(barcode);
            System.out.println();

            System.out.println("Enter product quantity You want to remove: ");
            int quantity = Integer.parseInt(in.readLine().trim());
            System.out.println();

            System.out.println("Enter product price You want to remove: ");
            double price = Integer.parseInt(in.readLine().trim());
            System.out.println();

            cart.removeItem(new SoldItem(item, quantity, price));
        }
        catch(SalesSystemException e){
            System.out.println(e.getMessage());
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            System.out.println("Check Your input");
        }
    }

    public boolean checkIfNotInList(long itemId, String itemName) {
        List<StockItem> whList = warehouse.fetchStockItems();
        for (StockItem whItem : whList) {
            if (whItem.getId() == itemId || whItem.getName().toLowerCase().equals(itemName.toLowerCase())) {
                return false; //item with same name or id was found in list
            }
        }
        return true; //item is not in list, true.
    }

    public void addStock() throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter bar code: ");
        Long barcode = Long.parseLong(in.readLine().trim());
        System.out.println();

        System.out.println("Enter product name: ");
        String productName = in.readLine().trim();
        System.out.println();

        //Check that product isn't already in list
        if (checkIfNotInList(barcode, productName)) {
            System.out.println("Enter quantity of product: ");
            int productQuantity = Integer.parseInt(in.readLine().trim());
            System.out.println();
            System.out.println("Enter price of product: ");
            double productPrice = Double.parseDouble(in.readLine().trim());
            System.out.println();
            //if-else statement is hacky check
            if (productQuantity >= 0 && productPrice >= 0) {
                warehouse.addItem(new StockItem(barcode, productName, "", productPrice, productQuantity));
                System.out.println("########### SUCCESS #########");
            } else {
                System.out.println("Failed to add new item to warehouse, price and quantity cannot be negative values!");
            }
        } else {
            System.out.println("Product already in stock!");
        }
    }

    public void modifyStock() throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter bar code: ");
        String barcode = in.readLine().trim();
        long barCode = -1;
        if (!barcode.equals("")) {
            barCode = Long.parseLong(barcode);
        }
        System.out.println();

        System.out.println("Enter product name: ");
        String productName = in.readLine().trim();
        System.out.println();

        //Check that product is already in list
        if (!checkIfNotInList(barCode, productName)) {

            StockItem currentItem = getStockItemFromInput(barcode, productName);

            System.out.println("Enter quantity of product (currently " + currentItem.getQuantity() + "): ");
            String enteredQuantity = in.readLine().trim();
            if (!enteredQuantity.equals("")) {
                warehouse.changeItemQuantity(currentItem, Integer.parseInt(enteredQuantity));
            }
            System.out.println();

            System.out.println("Enter price of product (currently " + currentItem.getPrice() + "): ");
            String enteredPrice = in.readLine().trim();
            if (!enteredPrice.equals("")) {
                warehouse.changeItemPrice(currentItem, Double.parseDouble(enteredPrice));
            }
            System.out.println();
            System.out.println("########### SUCCESS #########");
        } else {
            System.out.println("Product not in stock!");
        }
    }

    //Method for getting StockItem from input data
    public StockItem getStockItemFromInput(String barcode, String name) {
        log.debug("Searching for " + barcode + ": " + name + " in Stock");
        //Get the existing stockItem

        StockItem inStockItem = null;
        if (!barcode.equals("")) {
            log.debug("Searching by barcode");
            try {

                inStockItem = warehouse.fetchStockItem(Long.parseLong(barcode));

            } catch (NumberFormatException e) {
                log.debug("Parsing of barcode was unsuccessful");
            }
        }
        //The product was found not found by ID, secondary search by Name
        if (inStockItem == null) {
            log.debug("Searching by product name");

            inStockItem = warehouse.fetchStockItem(name);

        }
        return inStockItem;
    }

    public boolean paymentProcedure(Payment payment) {
        //Ask payment method
        String input = "";
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while(!input.equals("cash") && !input.equals("credit") && !input.equals("cancel")){
            System.out.println("Enter payment method (cash/credit/cancel):");
            try{
                input = in.readLine().trim().toLowerCase();
            }
            catch(IOException e){
                break;
            }
        }
        //If paying with cash
        if(input.equals("cash")){
            log.debug("Cash payment chosen");
            System.out.println("The total price of shopping cart: "+payment.getAmountToPay());
            payment.setCashPayment(true);
            System.out.println("Enter the amount of money Customer has given or cancel:");
            try{
                float moneyGiven = -1;
                while(moneyGiven<payment.getAmountToPay() && !input.equals("cancel")){
                    System.out.println("Cash given must be greater or even than shopping cart price");
                    input = in.readLine().trim().toLowerCase();
                    moneyGiven = Float.parseFloat(input);
                }
                System.out.println("Return to Customer: "+payment.calculateReturn(moneyGiven)+" Euros");
                return true;
            }
            catch(Exception e){
                log.error("Exception in paymentProcedure Cash payment");
            }

        }
        else if(input.equals("credit")){
            log.debug("Credit payment chosen");
            System.out.println("The total price of shopping cart: "+payment.getAmountToPay());
            System.out.println("Enter PIN:");
            try{
                int tries = 0;
                input = in.readLine().trim().toLowerCase();
                while(true){
                    try{

                        if(input.equals("cancel")){
                            return false;
                        }

                        if(payment.checkPIN(input)){
                            return true;
                        }
                        tries++;
                        System.out.println("Wrong pin was entered. Please try again.");
                        if(tries>3){
                            System.out.println("More than 3 times wrong PIN was entered");
                            System.out.println("Transaction cancelled");
                            return false;
                        }
                        input = in.readLine().trim().toLowerCase();
                    }
                    catch(Exception e){}

                    log.error("Exception in paymentProcedure Credit payment");

                }
            }
            catch(Exception e){
                log.error("Exception in paymentProcedure Credit payment");
            }
        }
        else{
            System.out.println("Payment procedure cancelled");

        }
        return false;
    }

    public List<Employee> getEmployeeList () {
        List<Employee> foundList = employeeLog.getEmployeeList();
        return foundList;
    }

    public void loyalCostumerCheck(ShoppingCart cart) throws IOException{
        String input = "";
        System.out.println("Enter Customer name: ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String customerName = in.readLine().trim();
            System.out.println("Enter Customer ID: ");
            long customerID = Long.parseLong(in.readLine().trim());
            Customer checkedCustomer = cart.checkCustomer(new Customer(customerName,
                    customerID, ""));
            if(checkedCustomer!=null) {
                System.out.println("Loyal Customer registered");
            }
            else {
                System.out.println("Loyal Customer not found");
            }
    }

    public String getTeamAsString() {
        return meeskond.toString();
    }
}
