package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class History {

    private final SalesSystemDAO dao;

    public History(SalesSystemDAO dao){
        this.dao=dao;
    }

    public List<Purchase> getPurchases(){
        dao.beginTransaction();
        List<Purchase> purchases = dao.getPurchaseList();
        dao.commitTransaction();
        return purchases;
    }

    public List<Purchase> getLast10Purchases(){
        dao.beginTransaction();
        List<Purchase> purchasesList = dao.getLast10Purchases();
        dao.commitTransaction();
        return purchasesList;
    }

    public List<Purchase> getItems(String filter){
        dao.beginTransaction();
        List<Purchase> results = dao.getPurchaseList().stream().filter(item -> item.getId().toString().equals(filter) || item.getPaymentMethod().toLowerCase().contains(filter.toLowerCase())).collect(Collectors.toList());
        dao.commitTransaction();

        return results;
    }

    public List<Purchase> getPurchasesBetweenDates(Date startDate,Date endDate){
        //Adding one day to include last day in range
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();

        dao.beginTransaction();
        List<Purchase> inRangePurchasesList = dao.getPurchasesBetweenDates(startDate,endDate);
        dao.commitTransaction();
        return inRangePurchasesList;
    }

    public LinkedHashMap<String, Number> generateFigure(LocalDate localStartDate, LocalDate localEndDate){

        Date startDate = localDatetoDate(localStartDate);
        Date endDate = localDatetoDate(localEndDate);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);

        //Get all dates in range
        ArrayList<String> datesInRange = new ArrayList<>();
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);


        while(startCalendar.before(endCalendar)){
            Date date = startCalendar.getTime();
            datesInRange.add(dateFormat.format(date));
            startCalendar.add(Calendar.DATE,1);
        }
        //also add one more day because it starts from 0. if you want to show a graph for 5.nov-5.nov at the moment, arr length is 0.
        Date date = startCalendar.getTime();
        datesInRange.add(dateFormat.format(date));

        //Adding one day to include last day in range from DAO Query
        endCalendar.add(Calendar.DATE,1);
        dao.beginTransaction();
        List<Purchase> purchases = dao.getPurchasesBetweenDates(startDate,endCalendar.getTime());
        dao.commitTransaction();

        //Sort array by purchase date
        purchases.sort(Purchase.getGetpurchaseComparator());

        //cash flow by day
        double[] cashFlowByDay = new double[datesInRange.size()];

        //Go through each purchase
        for(Purchase purchase: purchases){
            String currentDate = dateFormat.format(purchase.getDate());
            if(purchase.getDate().getTime()>startDate.getTime() && purchase.getDate().getTime()<endCalendar.getTime().getTime()){
                //Format date
                //Go through days in range
                for(int i=0;i<datesInRange.size();i++){
                    if(datesInRange.get(i).equals(currentDate)){
                        cashFlowByDay[i]+=purchase.getSum();
                    }
                }
            }
        }

        LinkedHashMap<String, Number> hashmap = new LinkedHashMap<>();
        //Add data to list

        for(int i=0;i<datesInRange.size();i++){
            hashmap.put(datesInRange.get(i), cashFlowByDay[i]);
        }
        return hashmap;
    }

    public static Date localDatetoDate (LocalDate dateToConvert) {
        return Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }


}
