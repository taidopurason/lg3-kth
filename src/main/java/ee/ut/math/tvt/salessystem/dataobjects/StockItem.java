package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Stock item.
 */
@Entity
@Table(name = "STOCKITEM")
public class StockItem {

    @Id
    private Long id;
    @Column
    private String name;
    @Column
    private double price;
    @Column
    private String description;
    @Column
    private int quantity;
    @OneToMany(mappedBy = "stockitem", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ExpiryDate> expiryDates = new ArrayList<>();
//    @Transient //Not implemented
//    private List<LocalDate> expiryDates;

    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, double price, int quantity, List<ExpiryDate> expiryDates) {
        this(id, name, desc, price, quantity);
        this.expiryDates = expiryDates;
    }

    public StockItem(Long id, String name, String desc, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
        this.expiryDates = new ArrayList<>();
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<LocalDate> getExpiryDates() {
        ArrayList<LocalDate> dates = new ArrayList<>();
        for(int i=0;i<expiryDates.size();i++){
            dates.add(expiryDates.get(i).getExpiryDate());
        }
        return dates;
    }

    public void addExpiryDate(ExpiryDate expiryDate){
        expiryDates.add(expiryDate);
        expiryDate.setStockitem(this);
    }

    public ExpiryDate getExpiryDate(LocalDate date){
        for(int i=0;i<expiryDates.size();i++){
            if(date.equals(expiryDates.get(i).getExpiryDate())){
                return expiryDates.get(i);
            }
        }
        return null;
    }

    public void removeExpiryDate(ExpiryDate date){
        expiryDates.remove(date);
    }

    @Override
    public String toString() {
        return String.format("StockItem{id=%d, name='%s'}", id, name);
    }
}
